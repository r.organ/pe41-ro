/* TASK - 2
 * Create a function, that will:
 * take an elements from the page with class 'training-list', and text content equals 'list-element 5'.
 * Show this element in console.
 * Replace text content in this element to "<p>Hello</p>" without creating a new HTML element on the page
 * Use array methods to complete the task.
 * */

function changeContentList () {
    const listItems = document.querySelectorAll('.training-list')
    listItems.forEach(function (item) {
        if (item.textContent === 'list-element 5') {
            //if (item.includes('list-element 5')) {
        item.innerHTML = '<p>Hello</p>'
    }
    })
}
changeContentList()