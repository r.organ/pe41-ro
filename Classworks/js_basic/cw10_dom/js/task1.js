/* TASK - 1
 * Get several elements from the page, by:
 *   tag
 *   class
 *   identifier
 *   CSS selector
 * Use console.dir() method to show up the elements
 * */

let tag = document.getElementsByTagName('ul')
let clas = document.getElementsByClassName('training-list')
let iden = document.getElementById('list')
let csssel = document.querySelector('.training-list')
console.dir(csssel)