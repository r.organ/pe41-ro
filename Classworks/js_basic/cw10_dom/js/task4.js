/* TASK - 4
 * There is a list of items on the user screen.
 * Create a function that will find the 'run out' items. The amount of 'run out' items is equal to 0.
 * Replace the 0 inside text content of those elements to 'run out' and change the text color to red.
 */

function checkCountGood (){
    const listGoods= document.querySelectorAll('.storage-item')
    listGoods.forEach(item => {
        const splitContent = item.textContent.split('- ')
        // console.log(splitContent)
        if (splitContent[1] === '0' ) {
            item.style.color = 'red';
            item.textContent = `${splitContent[0]} - run out`
        }
    })
}

checkCountGood()