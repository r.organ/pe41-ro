/* TASK - 3
 * Get an element with class 'remove-me' and remove it from the page.
 * Find element with class 'make-me-bigger'. Replace class 'make-me-bigger' to 'active'. Class 'active' already exists in CSS.
 * */


const buttonRemoveMe = document.querySelector('.remove-me');
// buttonRemoveMe.remove();
// buttonRemoveMe.style.display = 'none';
// buttonRemoveMe.outerHTML = '';
const buttonMakemeBigger = document.querySelector('.make-me-bigger')
buttonMakemeBigger.className = 'active';