/*TASK 5
* Create a function that takes string with date 'DD/MM/YYYY' as an argument.
*
* Return value: number of weekday of the first day in this month
* */

function getFirstDayinMoth(data) {
    const formatDate = data.split('/').reverse().join('.')

    const date = new Date(formatDate)

    return date.getDay()

}


getFirstDayinMoth('12/10/1994')

