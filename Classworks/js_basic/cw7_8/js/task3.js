/* TASK 3
* Create a function cutMaxLength(string, maxlength)
* You need to cut the exact number of characters(maxLength) from source string and + "...".
* Return value:
*       if the number of the characters in source string is bigger then maxLength - the string that has been cut
*       if the number of the characters in source string is smaller then maxLength - the source string itself
*/

const sourceString =
    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A architecto atque dignissimos distinctio earum est facere facilis fuga, ipsa labore laborum obcaecati possimus qui ratione similique suscipit tempora temporibus totam.'
const sourceStringSecond = 'Lorem ipsum dolor sit amet'


function cutMaxLength(string, maxlength){
    let result;
    if (string.length > maxlength) {
       result = string.substring(0, maxlength) + '...'
    } else result = string

    return result
}

console.log(cutMaxLength(sourceString, 10))
console.log(cutMaxLength(sourceStringSecond, 30))
