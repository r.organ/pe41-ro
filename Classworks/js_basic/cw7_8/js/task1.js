/*TASK 1
 * Write a function customCharAt(string,index)
 *   string - source string
 *   index - the index of the particular character of the string that we need
 * Return value: the character itself, that is placed on the specific index
 * */


function customCharAt(string, index) {

    return string.charAt(index);

}

console.log(customCharAt('specific', 5))