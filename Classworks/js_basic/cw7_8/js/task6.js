/* TASK - 1
 * Create sum() function.
 * Arguments: two integer numbers
 * Return value: sum of two arguments
 * */

// const summ = (firstNum, secNum) => firstNum + secNum;
//
// console.log(summ)

/* TASK - 2
* Write a functions, that will be counting from start to end of range.
* Arguments: start of the range, end of the range
*/

// const count = (startRange, endRange) => {
//     for (let i=startRange; i <= endRange; i++ ) {
//         console.log(i)}
// }
//
// count(2,5)


/* TASK - 3
 * Write a function, that will sum up all the arguments passed into it.
 * */



const summAllArgs = (...args) => {
let result = 0;
for (const singleNum of args) {
    result += singleNum
}
return result
}

summAllArgs(12,45,544,45,6,7,567,56,45,34,324,2)