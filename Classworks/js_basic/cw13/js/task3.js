/*TASK - 3
 * Create a function, that will be changing the background color of the 100px square randomly,
 * by the clicking on it. Every color should be random, transparent and white are not included in the list of colors.
 */

const square = document.createElement('div');
square.style.cssText = 'width: 100px; height: 100px; background-color: black';

const getRandomNumber = () => Math.floor(Math.random() * 255)


document.body.prepend (square);

square.addEventListener('click', ()=> {
    let rgb = `rgb(${getRandomNumber()}, ${getRandomNumber()}, ${getRandomNumber()})`
    square.style.backgroundColor = rgb
})