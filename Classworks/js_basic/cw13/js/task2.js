/**
 * Task - 02
 * Create a <p> element that should represent how many pixels the user had scrolled on the page from the top of it.
 * */

const p = document.createElement('p');

p.style.cssText = 'position:fixed;top: 40px; left: 50px; background:yellow;'

document.body.prepend(p);

window.addEventListener('scroll', function (){
   p.textContent = Math.floor(this.pageYOffset) +'px';

})