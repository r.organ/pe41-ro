/**
 * Task - 01
 * Create h1 element on a page using JavaScript. Place it on the page.
 * After any key on the page was pressed, print the key's name as the text content of the h1 element.
 * */

const title = document.createElement('h1');

document.body.prepend(title);

window.addEventListener('keyup', (event)=> {

    title.textContent += event.key
});

