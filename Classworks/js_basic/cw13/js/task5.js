/*TASK - 5
 * There is an input next to the 100px square.
 * Only HEX of the color can be entered in this input. Next to this input - 'Ok' btn is placed and its inactive by default.
 * Create a function that will be changing the color.
 * After hex is entered 'Ok' button should become active so user could press it.
 * After pressing the 'Ok' button color should changes.
 * */

function createElement (tag, content = "", styles = "") {
    const elem = document.createElement(tag);
    if (content) {
        elem.textContent = content;
    }
    if (styles) {
        elem.style.cssText = styles;
    }
    return elem
}

const square = createElement('div', '', 'width: 100px; height: 100px; background-color: black')
const field = createElement('input')
const btn = createElement('button', 'OK')

document.body.prepend(square, field, btn);
// btn.disabled = true;
//
// field.addEventListener('focus', () => {
//
// })

btn.addEventListener('click', function () {
    square.style.backgroundColor = field.value
})