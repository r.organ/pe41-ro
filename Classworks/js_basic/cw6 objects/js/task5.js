/* ЗАДАНИЕ - 5
* Дополнить функционал решения из прошлой задачи.
* Добавить для свойства price getter и setter.
* getter должен возвращать своство в виде строки в формате `${ЗНАК ВАЛЮТЫ}${ЦЕНА}`, где знаком валюты должен быть знак гривты.
* */

function createProductCart (title, code, price, description) {
    const card = {
        productTitle: title,
        productCode: code,
        productPrice: price,
        productDesc: description,
        
        startSale: function (discount) {
            this.productPrice = this.productPrice - (this.productPrice * discount / 100)
        },
    }
    return card
}

const newProductCard = createProductCart ('iphone', 222222, 400, 'the best smartphone ever')

console.log('old price', newProductCard)

newProductCard.startSale(20)

console.log('new price', newProductCard)