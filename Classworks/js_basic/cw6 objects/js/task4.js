/* Написать функцию createProductCart(). Которая создает объект карточки товара.
*
* Аргументы:
*  - title - название товара
*  - code - артикул товара
*  - price - цена
*  - description - описание
*
* Возвращаемое значение: объект карточки товара со всеми свойствами и методами
*
* Внутри функции нужно создать объект, записать в него све свойства и добавить один метод - startSale(), который изменяет цену товара и делает ее меньше на указанное количество процентов.
* метод startSale принимает аргумент - размер скидки, одним числом без знака процентов.
* */
function createProductCart (title, code, price, description) {
    const card = {
        productTitle: title,
        productCode: code,
        productPrice: price,
        productDesc: description,

        startSale: function (discount) {
            this.productPrice = this.productPrice - (this.productPrice * discount / 100)
        },
    }
    return card
}

const newProductCard = createProductCart ('iphone', 222222, 400, 'the best smartphone ever')

console.log('old price', newProductCard)

newProductCard.startSale(20)

console.log('new price', newProductCard)