/* ЗАДАНИЕ - 3
 * Добавить к предыдущему заданию функционал.
 * В возвращаемом объекте должен появиться еще один метод - addField(). Он будет добавлять свойства в объект.
 * Т.е. внутри объекта будет еще одно свойство\ключ\поле, значением которого будет являться функция.
 * Эта функция принимает два аргумента:
 *   1 - имя свойства, которое будет создаваться
 *   2 - значение. которое туда должно быть записано
 * */

function createUser (userName, userAge) {
    const user = {
        name: userName,
        age: userAge,
        addAge: function () {
            this.age ++
        },
        addField: function (key, value) {
            this[key] = value
        },
    }
    return user
}

const newUser = createUser(prompt('enter name'), +prompt('enter age'))

console.log(newUser)
newUser.addAge()
newUser.addField('Hobby', 'Football')
newUser.addField('car', 'bmw')
console.log(newUser)