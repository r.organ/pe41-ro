/* ЗАДАНИЕ - 2
 * Добавить к предыдущему заданию функционал.
 * В возвращаемом объекте должен быть метод, который увеличивает возраст на 1.
 * Т.е. внутри объекта будет свойство\ключ\поле, значением которого будет являться функция,
 * которая увеличивает свойство\ключ\поле age ЭТОГО объекта на 1
 * */

function createUser (userName, userAge) {
    const user = {
        name: userName,
        age: userAge,
        addAge: function () {
            this.age ++
        },
    }
    return user
}

const newUser = createUser(prompt('enter name'), +prompt('enter age'))

console.log(newUser)
newUser.addAge()
console.log(newUser)