/* ЗАДАНИЕ - 1
 * Написать функцию, которая принимает 2 аргумента - имя и возраст пользователя
 * Возвращаемое значение этой функции - объект с двумя ключами name и age, куда будут записаны значения переданных функции аргументов.
 * */

function createUser (userName, userAge) {
    const user = {
        name: userName,
        age: userAge,
    }
    return user
}

const newUser = createUser(prompt('enter name'), +prompt('enter age'))
console.log(newUser)