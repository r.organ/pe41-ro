/* TASK - 1
 * Create a square.
 * Ask the user about the size and background color of the square.
 * Create an element in JS
 * Ask the size.
 * Ask the background color.
 * Add the styles to the element in JS
 * Place the element BEFORE the first script tag on your page
 */
//Example 1
// let squareSize = prompt('enter a square size');
// let squareColor = prompt('enter a color of square');
//
// let square = document.createElement('div');
//
// square.style.backgroundColor = squareColor;
// square.style.width = squareSize + 'px';
// square.style.height = squareSize + 'px';
//
// let script = document.querySelector('script')
// script.before(square)

//Example 2
let squareSize = prompt('enter a square size');
let squareColor = prompt('enter a color of square');

function createSquare(size, color, parent) {
    let square = document.createElement('div');
    square.style.cssText = `width:${size}px; height:${size}px; background-color:${color}`;
    parent.before(square)
}

let script = document.querySelector('script')
createSquare(squareSize,squareColor,script)