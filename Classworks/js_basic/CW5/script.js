/* ЗАДАНИЕ - 1
 * Написать функцию суммирования.
 * Принимает аргументы: первое число и второе число
 * Возвращаемое функцией значение: сумма двух аргументов
 * */

// function summ(a, b) {
//     return a + b
// }
// let result = summ (3,5 )
// console.log(result)

/* ЗАДАНИЕ - 2
 * Написать функцию которая будет принимать два аргумента - число с которого начать отсчет и число до которого нужно досчитать.
 * Под отсчетом имеется в виду последовательный вывод чисел в консоль с увеличением на единицу.
 */

// function showNumbers(firstNumber, lastNumber) {
//     if (firstNumber < lastNumber) {
//         for (let i = firstNumber; i <= lastNumber; i++) {
//             console.log(i)}
//     } else console.log ('first number is bigger than last one')
// }
//
// showNumbers(10,5)

/* ЗАДАНИЕ - 3
 * Написать функцию, которая будет суммировать ВСЕ числа, которые будут переданы ей в качестве аргументов.
 * */

// function summAllargs() {
//     let total = 0
//
//     for (let i = 0; i < arguments.length; i++) {
//         total += arguments[i];
//     }
//     return total
// }
//
// let summ = summAllargs(12, 40, 55, 2)
//
// document.write(summ)

/* ЗАДАНИЕ - 4
 * Написать функцию, которая из всех переданных аргументов возвращает только выбранный тип данных.
 * Аргументы:
 *   1 - выбранный тип данных, который нужно отоборать
 *   2 и далее - елементы, из которых нужно отобрать
 *
 * ДОП. ЗАДАНИЕ: Написать вторую реализацию, где элементы из которых нужно отбирать переданы в массиве.
 * */

// function filterDataType (dataType, ...otherArgs) {
//     const filterResult = [];
//     for (let i = 0; i < otherArgs.length; i++) {
//        if (typeof otherArgs[i] === dataType) {
//            filterResult.push(otherArgs[i])
//        }
//     }
//     return filterResult
// }
//
// let res = filterDataType('string', 0, null, 'saasas', 555, [], {}, undefined, 2, 'sdff')
// console.log(res)


/* ЗАДАНИЕ - 5
* Написать функцию getMultipleOf, которая будет выводить в консоль ВСЕ числа в заданом диапазоне, которые кратны третьему аргументу.
* Аргументы функции:
*   1) число С которого начинается диапазон (включительно)
*   2) число которым диапазон заканчивается (включительно)
*   3) число КРАТНЫЕ КОТОРОМУ нужно вывести в консоль
* Возвращаемое значение - отсутствует
* */

function getMultipleOf (start, end, divide) {
    if(divide === 0) return null
    if (start >end) {
        let temp = start
        start = end
        end = temp
    }
    for (let i = start; i <= end; i++) {
        if (i % divide === 0) {
            document.write(`<li> ${i}</li>`)
        }
    }

}

getMultipleOf(13, 115, 10)