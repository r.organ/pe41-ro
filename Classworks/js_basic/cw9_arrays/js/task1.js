/* TASK - 1
 * Create a function that does not receive any arguments
 * it will only ask the user what dishes he prefers to eat on breakfast.
 * User can enter only one dish simultaneously, so you need to ask him UNTIL he will enter 'end'.
 * Each dish needs to be placed into an array.
 * Return value: array with all of the dishes.
 * */

function getBreakfest() {

let breakfestFood = [];
let userReply = prompt('Enter what dishes do you prefer to eat on breakfast: ')
    while (userReply !== 'end') {
    breakfestFood.push(userReply)
    userReply = prompt('Enter what dishes do you prefer to eat on breakfast: ')
    }

    return breakfestFood
}

console.log (getBreakfest())