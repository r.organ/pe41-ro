/* TASK - 2
 * Create a function that will take one argument - array from the previous task result.
 * The task is to show every dish from the array in the console and delete it from the source array.
 * After all of the dishes will be shown on the console, the array must be empty.
 * */

function getBreakfest() {

    let breakfestFood = [];
    let userReply = prompt('Enter what dishes do you prefer to eat on breakfast: ')
    while (userReply !== 'end') {
        breakfestFood.push(userReply)
        userReply = prompt('Enter what dishes do you prefer to eat on breakfast: ')
    }

    return breakfestFood
}

const morningBreakFast = ['coffee', 'tea', 'apple', 'juice']

function cleanArray (sourceArray) {
    const length = sourceArray.length;
    for(let i =0; i< length; i++){
    console.log(sourceArray.shift());
}
    console.log(sourceArray)
}

cleanArray(morningBreakFast)