/* ЗАДАНИЕ 1
 * Записать в переменную '123', вывести в консоль typeof этой переменной.
 * Преобразовать эту переменную в численный тип при помощи parseInt(), parseFloat(), унарный плюс +
 * После этого повторно вывести в консоль typeof этой переменной.
 * */

// let first_var = '123helllo'
//
// console.log (typeof first_var)
// //console.log(typeof (first_var))
//
// let transformInt = parseInt(first_var)
// console.log('transform ->parseInt', typeof transformInt)
//
// let transformFloat = parseFloat(first_var)
// console.log('transform ->parseFloat', typeof transformFloat)
//
// let transformPlus = +first_var
// console.log('transform -> унарный плюс', typeof transformPlus)

/* ЗАДАНИЕ 2
 * Вывести на экран уведомление с текстом "Hello! This is alert" при помощи модального окна alert
 * */

// const msg = 'Hello! This is alert'
// alert(msg)


/* ЗАДАНИЕ 3
 * Вывести на экран модальное окно prompt с сообщением "Enter the number".
 * Результат выполнения модального окна записать в переменную, значение которой вывести в консоль.
 * */

// let answer = prompt('Enter the number')
// console.log(answer)

/* ЗАДАНИЕ 4
 * При помощи модального окна prompt получить от пользователя два числа.
 * Вывести в консоль сумму, разницу, произведение, результат деления и остаток от деления их друг на друга.
 * */

// let number_one = parseInt(prompt ('Enter the number 1'))
// let number_two = parseInt(prompt('Enter the number 2'))
//
// let summ = number_one + number_two
// console.log(summ)
//
// let minus = number_one - number_two
// console.log(minus)
//
// let multiplication = number_one * number_two
// console.log(multiplication)
//
// let divide = number_one / number_two
// console.log(divide)
//
// let rem_of_div = number_one % number_two
// console.log(rem_of_div)


/* TASK 5
 * Use browser modal windows for getting three numbers from user.
 * Then execute into console:
 *   - arithmetic average
 *   - max number
 *   - min number
 * */

// let firstNum = +prompt('enter first num')
// let secondNum = +prompt('enter second num')
// let thirdNum = +prompt('enter third num')
//
//
// let avergae = (firstNum + secondNum + thirdNum )/ 3
// console.log('average ->', Math.round(avergae))
//
// let max = Math.max(firstNum, secondNum, thirdNum)
// console.log('max->', max)
//
// let min = Math.min(firstNum, secondNum, thirdNum)
// console.log('max->', min)

/* TASK 1
* Show up next things in console, using console.log() method:
  - max integer value
  - min integer value
  - not a number
  - max safe integer
  - min safe integer
* */
//
// console.log(Number.MAX_VALUE)
// console.log(Number.MIN_VALUE)
// console.log(Number.NaN)
// console.log(Number.MAX_SAFE_INTEGER)
// console.log(Number.MIN_SAFE_INTEGER)

/* TASK 2
* Create two variables. First will contain - any number, second - any string with any word inside.
* Execute the SUM of these two variables in the console
* Execute the DEFERENCE between these two variables in the console
* Execute the result of MULTIPLYING these two variables in the console
* Explain the result of every operation by your own words
* */

// const a = 5;
// const b = 'test';
//
// console.log(a + b)
// // в JS "плюс" используется для "объеденения" строчных значений. т.к. одна переменная, а  вторая текст, то результат не суммирование а "объеденение"
// console.log(a - b)
// // все осталыне мат операторы, кроме + работают в стандартном режиме, и если в строке цифра, то она будет преобразована в цифру и будет получен результат. Если текст, то результат будет НаН.
// console.log(a * b)
// // все осталыне мат операторы, кроме + работают в стандартном режиме, и преобразоывают строку в цифру.

/* TASK 3
 * Create 2 variables. Assign true for first one, and false for second one.
 * Execute into console:
 *   - result of comparing 0 and variable with false inside with == operator
 *   - result of comparing 1 and variable with true inside with === operator
 *   - Explain the result
 * */

// let bool1 = true
// let bool2 = false
//
// console.log(0 == bool2)
// // можно сравнить с бинарной системой, где 0 - false, 1 - true. При сравнении 0 с false, результат возращает true, т.к. не строгое совпадение.
// console.log(1 === bool1)
// // в этом случае возвращает false, т.к. нет строго совпадения по типу данных.


let x = 6;
let y = 15;
let z = 4;

