//example 1
// const showModalBtn = document.createElement('button');
// showModalBtn.textContent = 'show modal'
//
// document.getElementById('root').append(showModalBtn);
//
// const modal  = document.querySelector('.modal-wrapper');
//
// showModalBtn.addEventListener('click', () =>{
//     modal.style.display = 'flex'
//
//     modal.addEventListener('click', function(ev){
//         if (ev.target === this || ev.target.classList.contains('modal-close')) {
//             this.style.display = 'none'
//         }
//     })
// })

//example 2
const showModalBtn = document.createElement('button');
showModalBtn.textContent = 'show modal'

document.getElementById('root').append(showModalBtn);

showModalBtn.addEventListener('click', () => {
    // create modal
    const modal = createModal('первая модалка')

    document.body.append(modal)
})

function createModal(content) {
    const modalWrapper = document.createElement('div')
    modalWrapper.className = 'modal-wrapper'

    modalWrapper.insertAdjacentHTML('afterbegin',
        `<div class="modal">
                  <button class="modal-close">x</button>
                    <p class="modal-text">
                    ${content}
                    </p>
                </div>`,
        )
    modalWrapper.addEventListener('click', function(event){
        if (event.target === this || event.target.classList.contains('modal-close')) {
             this.remove()}

    })
    return modalWrapper
}
