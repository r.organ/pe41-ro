/**
 * Task - 04
 *
 * Recreate a pack-man on the page. And make it movable.
 *
 * Make user controls direction of pack-man movement by pressing W, S, A, D keys.
 *
 *  W - pack-man moves 20px to top from it's current position;
 *  S - pack-man moves 20px to bottom from it's current position;
 *  A - pack-man moves 20px to the left from it's current position;
 *  D - pack-man moves 20px to the right from it's current position;
 * */


const packman = document.querySelector('.packman');
let currentX = 0;
let currentY = 0;

window.addEventListener('keydown', function (event) {
    if (event.key === 'd') {
        currentX += 20;
        packman.style.left = currentX + 'px';
        packman.style.transform = 'rotate(0deg)';
    }
    if (event.key === 'a') {
        currentX -= 20;
        packman.style.left = currentX + 'px';
        packman.style.transform = 'rotateY(180deg)';
    }
    if (event.key === 's') {
        currentY += 20;
        packman.style.top = currentY + 'px';
        packman.style.transform = 'rotate(90deg)';
    }
    if (event.key === 'w') {
        currentY -= 20;
        packman.style.top = currentY + 'px';
        packman.style.transform = 'rotate(270deg)';
    }
})

