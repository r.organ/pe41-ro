// const hoverBlocks = {
//     graphic: `<div class="hover-block">
//         <img class = "hover-icon" src="./images/icon-hover-our-work.png" alt="">
//         <div class="text-wrapper">
//         <p class="hover-title">creative design</p>
//         <p class = "hover-subtitle">graphic design</p>
//         </div>
//     </div>`,
//     design: `<div class="hover-block">
//         <img class = "hover-icon" src="./images/icon-hover-our-work.png" alt="">
//         <div class="text-wrapper">
//             <p class="hover-title">creative design</p>
//             <p class = "hover-subtitle">web design</p>
//         </div>
//     </div>`,
//     wordpress: `<div class="hover-block">
//         <img class = "hover-icon" src="./images/icon-hover-our-work.png" alt="">
//         <div class="text-wrapper">
//         <p class="hover-title">creative design</p>
//         <p class = "hover-subtitle">wordpress</p>
//         </div>
//     </div>`,
//     landing: `<div class="hover-block">
//         <img class = "hover-icon" src="./images/icon-hover-our-work.png" alt="">
//         <div class="text-wrapper">
//         <p class="hover-title">creative design</p>
//         <p class = "hover-subtitle">landing pages</p>
//         </div>
//     </div>`,
// }
// const root = document.getElementById('root')
//
// for (let i = 1; i <= 3; i++) {
//     root.insertAdjacentHTML(
//         'afterbegin',
//         `<div class="root-item">
//             ${hoverBlocks.graphic}
//             <img src="./images/portfolio/graphic-design/graphic-design${i}.jpg" alt="">
//         </div>`,
//     )
//     root.insertAdjacentHTML(
//         'afterbegin',
//         `<div class="root-item">
//             ${hoverBlocks.design}
//             <img src="./images/portfolio/web-design/web-design${i}.jpg" alt="">
//         </div>`,
//     )
//     root.insertAdjacentHTML(
//         'afterbegin',
//         `<div class="root-item">
//             ${hoverBlocks.wordpress}
//             <img src="./images/portfolio/wordpress/wordpress${i}.jpg" alt="">
//         </div>`,
//     )
//     root.insertAdjacentHTML(
//         'afterbegin',
//         `<div class="root-item">
//             ${hoverBlocks.landing}
//             <img src="./images/portfolio/landing-pages/landing-pages${i}.jpg" alt="">
//         </div>`,
//     )
// }
// for (let i = 4; i <= 6; i++) {
//     root.insertAdjacentHTML(
//         'afterbegin',
//         `<div class="root-item hidden">
//             ${hoverBlocks.graphic}
//             <img src="./images/portfolio/graphic-design/graphic-design${i}.jpg" alt="">
//         </div>`,
//     )
//     root.insertAdjacentHTML(
//         'afterbegin',
//         `<div class="root-item hidden">
//             ${hoverBlocks.design}
//             <img src="./images/portfolio/web-design/web-design${i}.jpg" alt="">
//         </div>`,
//     )
//     root.insertAdjacentHTML(
//         'afterbegin',
//         `<div class="root-item hidden">
//             ${hoverBlocks.wordpress}
//             <img src="./images/portfolio/wordpress/wordpress${i}.jpg" alt="">
//         </div>`,
//     )
//     root.insertAdjacentHTML(
//         'afterbegin',
//         `<div class="root-item hidden">
//             ${hoverBlocks.wordpress}
//             <img src="./images/portfolio/landing-pages/landing-pages${i}.jpg" alt="">
//         </div>`,
//     )
// }
// document.getElementById('showmore').addEventListener('click', () => {
//     const allHiddenImgs = document.querySelectorAll('.hidden')
//
//     allHiddenImgs.forEach(singleImg => {
//         singleImg.classList.remove('hidden')
//     })
//
// })

//
// const swiper = new Swiper('.swiper', {
//     // Optional parameters
//     direction: 'horizontal',
//     loop: true,
//
//     // If we need pagination
//     // pagination: {
//     //     el: '.swiper-pagination',
//     // },
//
//     // Navigation arrows
//     navigation: {
//         nextEl: '.swiper-button-next',
//         prevEl: '.swiper-button-prev',
//     },
//
//     // // And if we need scrollbar
//     // scrollbar: {
//     //     el: '.swiper-scrollbar',
//     // },
// })
let galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 10,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    loop: true,
    loopedSlides: 4
});
let galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 10,
    centeredSlides: true,
    slidesPerView: 'auto',
    touchRatio: 0.2,
    slideToClickedSlide: true,
    loop: true,
    loopedSlides: 4
});
galleryTop.controller.control = galleryThumbs;
galleryThumbs.controller.control = galleryTop;