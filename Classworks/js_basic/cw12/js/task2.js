/*TASK - 2
* Show alert with message 'This is mouseover', after the hover on the 'Click me' button.
*/

const btn = document.createElement('button');
const text = document.createElement('p');

btn.textContent = 'Show text';
text.textContent  = 'this is text';

document.body.prepend(btn, text);
text.style.cssText = 'opacity:0; transition: 0.5s';

btn.addEventListener('mouseover', ()=> {
    text.style.opacity = 1
})

btn.addEventListener('mouseout', ()=> {
    text.style.opacity = 0
})

