/*TASK - 1
 * Show alert with message 'This is click', after the click on the 'Click me' button.
 */

const btn = document.createElement('button');
btn.textContent = 'Click me';

btn.addEventListener('click', handleAlert);

document.body.prepend(btn)

function handleAlert() {
    alert('This is click')
}