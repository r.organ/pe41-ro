/**
 * Написати функціонал калькулятору курсу валют використовуючи API https://freecurrencyapi.net

 Технічні вимоги

 при успішному розрахунку результат показується у блоку нижче
 якщо приходять помилки, то відображати їх також нижче


 Advanced

 розкоментувати інпут дати в html та робити розрахунок курсу валют на вказану дату
 додати можливість обчислення всіх можливих курсів валют, що надає API

 для цього при завантаженні сторінки робити запит, щоб отримати всі можливі валюти, а потім рендерити селект компоненти з цими валютами**/

const form = document.querySelector('#currency-form');
const divResult = document.querySelector('.result');
const currencyTo = document.getElementById('currency-2');
const currencyFrom = document.getElementById('currency-1');

fetch('http://api.exchangeratesapi.io/v1/latest?access_key=c5bed70ad91d8faaec3f1f332d6f1434&format=1')
    .then(response => response.json())
    .then((result) => {
        const currencyList = Object.keys(result.rates);
        const currencyElems = currencyList.map((currencyName) => `<option value="${currencyName}">${currencyName}</option>`).join('');
        currencyTo.innerHTML = currencyElems;
        currencyFrom.innerHTML = currencyElems;
    })

form.addEventListener('submit', event => {
    event.preventDefault();
    const currencyToValue = currencyTo.value;
    const currencyFromValue = currencyFrom.value;
    const amount = document.getElementById('amount').value;
    fetch('http://api.exchangeratesapi.io/v1/latest?access_key=c5bed70ad91d8faaec3f1f332d6f1434&format=1')
        .then(response => response.json())
        .then((result) => {
            let rate = 1 / result.rates[currencyFromValue] * result.rates[currencyToValue] * amount;
            divResult.innerText = `${amount} ${currencyFromValue} is equal ${rate} ${currencyToValue}`;
        })
})

