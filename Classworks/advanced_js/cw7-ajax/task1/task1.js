/**
 * Вивести список усіх користувачів використовуючи ресурс https://ajax.test-danit.com/api-pages/jsonplaceholder.html

 Технічні вимоги

 вивести імʼя
 вивести юзернейм, при кліку на який створюється новий email лист
 вивести місто, вулицю та квартиру через кому в один блок
 вивести вебсайт, при кліку на який відкривається нова вкладка браузера
 * **/

const url = 'https://ajax.test-danit.com/api/json/users'

fetch(url)
.then(response => response.json())
.then(users => {
   const userName = users.map((user)=> `<li>${user.name}</li>`).join('');
   const ul = document.createElement('ul');
   const root = document.querySelector('#users');
   root.append(ul);
   ul.innerHTML = userName;
})

