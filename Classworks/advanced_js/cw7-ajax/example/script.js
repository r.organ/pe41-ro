console.log('before');
fetch('https://ajax.test-danit.com/api/json/todos')
    .then(response => {
        console.log(response);
        return response.json()
    })
    .then(todos => {
        console.log(todos);
        const ul = document.createElement('ul');
        const html = todos.map(todo => {
            return `<li style="text-decoration: ${todo.completed ? 'line-through' : 'none'}">${todo.title}</li>`
        }).join('')
        ul.innerHTML = html
        document.body.append(ul)
    })
console.log('after');
