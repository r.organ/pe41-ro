/**
 * Написати функціонал для тултипів
 *
 * Налаштування:
 * - текст
 * - позиціонування (зверху, справа, знизу, зліва), яке за замовчуванням зверху
 * - можливості відображення (при наведенні чи при кліку), за замовчуванням при наведенні
 *
 */

class Tooltip {
    constructor(targetSelector, text, placement, trigger = 'hover') {
        this.targetSelector = targetSelector;
        this.text = text;
        this.placement = placement;
        this.trigger = trigger;
        this.button = document.querySelector(targetSelector);
        this.button.classList.add('tooltip', `tooltip-${placement}`);
        this.isShow = false;
        this.enabled = true;
        this.renderContent();
        this.attachListeners();
    }
    attachListeners() {
        if (this.trigger === 'hover') {
            this.button.addEventListener('mouseover', () => {
                this.show();
            });
            this.button.addEventListener('mouseleave', () => {
                this.hide();
            });
        } else if (this.trigger === 'click') {
            this.button.addEventListener('click', () => {
                if (this.isShow) {
                    this.hide();
                } else {
                    this.show();
                }
            });
        }
    }
    renderContent() {
        this.tooltipContent = document.createElement('div');
        this.tooltipContent.classList.add('tooltip-content');
        this.tooltipContent.innerText = this.text;
        this.button.append(this.tooltipContent);
    }
    show() {
        if (this.enabled) {
            this.tooltipContent.style.display = 'block';
            this.isShow = true;
        }
    }
    hide() {
        this.tooltipContent.style.display = 'none';
        this.isShow = false;
    }
    enable() {
        this.enabled = true;
    }
    disable() {
        this.enabled = false;
    }
    deleteContent() {
        this.tooltipContent.remove();
    }

    changeText(value) {
        this.tooltipContent.innerText = value;
    }

    changePlacement(valuePlacement) {
        if (['up', 'right', 'down', 'left'].includes(valuePlacement)){
            this.button.className(`tooltip tooltip-${valuePlacement}`);
        }
    }
}

const showFirst = document.querySelector('#show-first');
const hideFirst = document.querySelector('#hide-first');
const disableThird = document.querySelector('#disable-first');
const enableThird = document.querySelector('#enable-first');
const disableAll = document.querySelector('#disable-all');
const enableAll = document.querySelector('#enable-all');
const applyButton = document.querySelector('#align-input')
const input = document.querySelector('#align-input');
const applyButton2 = document.querySelector('#apply-align2');
const input2 = document.querySelector('#align-input2');

const firstTooltip = new Tooltip('#btn-1', "It's first button", 'up');
const secondTooltip = new Tooltip('#btn-2', "It's second button", 'right');
const thirdTooltip = new Tooltip('#btn-3', "It's third button", 'down');
const fourthTooltip = new Tooltip('#btn-4', "It's fourth button", 'left', 'click');

const allToolTips = [firstTooltip, secondTooltip, thirdTooltip, fourthTooltip]

showFirst.addEventListener('click', () => {
    firstTooltip.show();
});

hideFirst.addEventListener('click', () => {
    firstTooltip.hide();
});

disableThird.addEventListener('click', () => {
    thirdTooltip.disable();
});

enableThird.addEventListener('click', () => {
    thirdTooltip.enable();
});

disableAll.addEventListener('click', () => {
    allToolTips.forEach(item => {
        item.disable();
    });
})

enableAll.addEventListener('click', () => {
    allToolTips.forEach(item => {
        item.enable()
    })
})

applyButton.addEventListener('click', () => {
    const value = input.value
    secondTooltip.changeText(value)
})

applyButton2.addEventListener('click', () => {
    fourthTooltip.changePlacement(input2.value)
})