// example with html Button
// button get id, text and type
// and render
class Button {
    constructor(id, text, targetContainer, clickFn) {
        const button = document.createElement('button')
        if (id) {
            button.setAttribute('id', id)
        }
        button.textContent = text
        this.button = button
        console.log(button);
        targetContainer.append(button)
        if (typeof clickFn === 'function') {
            this.button.addEventListener('click', event => {
                console.log(clickFn);
                clickFn(event)
            })
        }
    }

    changeText(newText) {
        this.button.textContent = newText
    }

    changeClassName(newClassName) {
        this.button.className = newClassName
    }
}

const rootDiv = document.querySelector('#root')
const buttonsGroupDiv = document.querySelector('.button-groups')

const saveButton = new Button('save-button', 'Save', buttonsGroupDiv)
const cancelButton = new Button('cancel-button', 'Cancel', buttonsGroupDiv)

const handleAddButtonClick = (event) => {
    console.log('clicked button', event);
    const input = document.querySelector('#text');
    const value = input.value
    const newButton = new Button(null, value, buttonsGroupDiv)
}

const handleApplyClassNames = () => {
    const input = document.querySelector('#text');
    const value = input.value
    saveButton.changeClassName(value)
    cancelButton.changeClassName(value)
}

const inputButton = new Button('input-button', 'Input', rootDiv)
const addButton = new Button('add-button', 'Add', rootDiv, handleAddButtonClick)
const applyClassNames = new Button('apply-class-names', 'Apply class', rootDiv, handleApplyClassNames)

console.log(inputButton);

const input = document.querySelector('#text');

input.addEventListener('input', (event) => {
    console.log('test')
    inputButton.changeText(event.target.value)
})
