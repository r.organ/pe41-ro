class Device {
    constructor(manufacturer, model, price) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.price = price;
    }
    getName () {
        console.log(`${this.manufacturer} ${this.model}`)
    }

    getSellPrice () {
        return this.price * 1.2
    }
}

class Laptop extends Device {
    constructor(manufacturer, model, price) {
        super(manufacturer, model, price)
    }

    getDeliveryPrice () {
        if (this.getSellPrice() > 30000 ) {
            return 0
        } else {
            return 200
        }
    }
}

class Phone extends Device {
    constructor(manufacturer, model, price, NFC = true) {
        super(manufacturer, model, price)
        this.hasNFC = NFC
    }

    getDeliveryPrice () {
        if (this.getSellPrice() > 10000 ) {
            return this.getSellPrice() * 0.01
        } else {
            return this.getSellPrice() * 0.03
        }
    }
}

class PremiumPhone extends Phone {
    constructor(manufacturer, model, price, NFC = true) {
        super(manufacturer, model, price, NFC)
    }

    getSellPrice () {
        return this.price * 1.35
    }
}

class Tablet extends Device {
    constructor(manufacturer, model, price, SIM = false) {
        super(manufacturer, model, price)
        this.hasSIM = SIM
    }

    getDeliveryPrice () {
        if (this.getSellPrice() > 10000 ) {
            return 0
        } else {
            return this.getSellPrice() * 0.01
        }
    }
}

const laptopHP007 = new Laptop('HP','007', 10000)
laptopHP007.getName()
console.log(laptopHP007)

const iphone10 = new Phone('Apple', 'Iphone 10', 30000)
const lgS1 = new Phone('LG', 'S1', 10000)
const xiamiMi5 = new Phone('Xiaomi', 'Mi5', 5000)
const nokiaN1 = new Phone('Nokia', 'N1', 8000, false)

const samsungXXL = new Tablet ('Samsung', 'XXL', 16000)

console.log(lgS1.getSellPrice())

const vertu1 = new PremiumPhone('Vertu', 'One', 10000)
console.log(vertu1.getSellPrice())