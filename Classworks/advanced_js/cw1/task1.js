/**
 * Наявний функціонал для друку оцінок одного студента
 * Але зі збільшенням кількості студентів постало питання про
 * розширення його
 * Для цього необхідно створити функцію-конструктор Student,
 * яка буде створювати об'єкт студента та мати ті ж самі методи
 *
 * - створити за допомогою функції-конструктора ще 2х студентів
 * - вивести оцінки кожного за допомогою метода printGrades
 * - вивести середній бал кожного студента
 * - додати метод, який буде виводити оцінку по заданій технологій
 * наприклад getGrade('html') повинен виводити оцінку студента по html
 * - вивести в консоль оцінку по js першого студента та по python - третього
 *
 *
 * ADVANCED:
 * - створити окремо функцію getStudentWithHighestResults, яка буде виводити
 * ім'я та прізвище студента за найвищим середнім балом
 */

// const student = {
//     firstName: 'Margie',
//     lastName: 'Sullivan',
//     sex: 'female',
//     grades: {
//         html: 90,
//         css: 60,
//         js: 50,
//         python: 45
//     },
//     printGrades() {
//         for (let key in this.grades) {
//             if (this.sex === 'male') {
//                 console.log(`По ${key} ${this.firstName} отримав ${this.grades[key]}`);
//             } else {
//                 console.log(`По ${key} ${this.firstName} отримала ${this.grades[key]}`);
//             }
//         };
//     },
//     average: function () {
//         let sum = 0;
//         let subjectsCount = 0;
//         for (const subject in this.grades) {
//             sum += this.grades[subject];
//             subjectsCount++;
//         }
//         return sum / subjectsCount;
//     }
// };
//
//
// student.printRating();
// student.average();

function Student (firstName, lastName, sex, grades) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.sex = sex;
    this.grades = grades;
}

Student.prototype.printGrades = function () {
    for (let key in this.grades) {
        if (this.sex === 'male') {
            console.log(`По ${key} ${this.firstName} отримав ${this.grades[key]}`);
        } else {
            console.log(`По ${key} ${this.firstName} отримала ${this.grades[key]}`);
        }
    }
}

Student.prototype.average = function () {
    let sum = 0;
    let subjectsCount = 0;
    for (const subject in this.grades) {
        sum += this.grades[subject];
        subjectsCount++;
    }
    return sum / subjectsCount;
}

Student.prototype.getGrade = function (subject) {
    console.log(this.grades[subject]);

}

Student.prototype.getFullName = function () {
    return `${this.firstName} ${this.lastName}`
}

const studentMargie = new Student ('Margie', 'Sullivan', 'female', {
   html: 90,
   css: 60,
   js: 50,
   python: 45,
});

studentMargie.printGrades();
console.log(studentMargie.average())

const studentJohn = new Student ('John', 'Smith', 'male', {
    html: 94,
    css: 65,
    js: 67,
    python: 76,
});

studentJohn.printGrades();
console.log(studentJohn.average())

function getStudentWithHighestResults(students) {
    console.log(students)
    let max = 0;
    let winner = '';
    for (const student of students) {
        if (student.average() > max) {
            max = student.average();
            winner = student.getFullName();
        }
    }
    console.log(max)
    console.log(winner)
}

getStudentWithHighestResults([studentJohn, studentMargie])