import UserCard from './js/user-card.js'
import Modal from './js/modal.js';
import NewUserForm from './js/new-user-form.js'

const tasksBoard = document.querySelector(".tasks-board");

const btnNewUser = document.querySelector('#btn-new-user');
btnNewUser.addEventListener('click', () => {
    let newUserModal
    const newForm = new NewUserForm((user) => {
        const { name, username, email, company: { name: companyname } = {}, id } = user;
        const cardUser = new UserCard(name, username, email, companyname, id)
        tasksBoard.append(cardUser.render());
        console.log(user);
        newUserModal.close();
    });

    newUserModal = new Modal({
        headerTitle: 'Add new user',
        body: newForm.render()
    });
    document.body.append(newUserModal.render());
})

async function displayUsers() {
    const response = await fetch(`https://ajax.test-danit.com/api/json/users/`)
    const users = await response.json();
    users.forEach(async user => {
        const { name, username, email, company: { name: companyname }, id } = user;
        const cardUser = new UserCard(name, username, email, companyname, id)
        tasksBoard.append(cardUser.render());

        const response = await fetch(`https://ajax.test-danit.com/api/json/users/${user.id}/todos`)
        const todos = await response.json();
        // console.log(todos);
        cardUser.renderTasks(todos);
    })
}

displayUsers()