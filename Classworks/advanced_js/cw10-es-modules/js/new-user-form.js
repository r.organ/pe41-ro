import Element from './element.js'
class NewUserForm extends Element {
    constructor(onSuccess) {
        super()
        this.onSuccess = onSuccess
    }

    sendUserData() {
        this.formElem.addEventListener('submit', (e) => {
            e.preventDefault();
            console.log(this.formElem.userFullName.value);
            const newUser = {
                name: this.formElem.userFullName.value,
                username: this.formElem.userNickName.value,
                email: this.formElem.userEmail.value
            }
            fetch(`https://ajax.test-danit.com/api/json/users`, {
                    method: "POST",
                    body: JSON.stringify(newUser)
                })
                .then(response => response.json())
                .then((user) => { this.onSuccess(user) })
        })
    }

    render() {
        this.formElem = document.createElement('form')
        this.formElem.innerHTML = `
        <div class="form-group">
            <label for="userFullName">Full Name</label>
            <input id="userFullName" class="form-control" name="userFullName" placeholder="Enter Name">
        </div>
        <div class="form-group">
            <label for="userNickName">Nick Name</label>
            <input id="userNickName" class="form-control" name="userNickName" placeholder="Enter Nick Name">
        </div>
        <div class="form-group">
            <label for="userEmail">Email</label>
            <input id="userEmail" class="form-control" name="userEmail" placeholder="Enter Email">
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
        `
        this.sendUserData();
        return this.formElem;
    }
}

export default NewUserForm