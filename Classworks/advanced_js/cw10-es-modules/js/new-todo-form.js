import Element from './element.js'
class NewUserForm extends Element {
    constructor(onSuccess) {
        super()
        this.onSuccess = onSuccess
    }

    sendUserData() {
    
    }

    render() {
        this.formElem = document.createElement('form')
        this.formElem.innerHTML = `
        <div class="form-group">
            <label for="taskName">Full Name</label>
            <input id="taskName" class="form-control" name="taskName" placeholder="Enter Name">
        </div>
        <button type="submit" class="btn btn-primary">Assign</button>
        `
        this.sendUserData();
        return this.formElem;
    }
}

export default NewUserForm