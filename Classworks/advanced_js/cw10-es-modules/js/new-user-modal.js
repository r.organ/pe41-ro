import Element from './element.js'
import NewUserForm from './new-user-form.js';
import UserCard from './user-card.js';

const BODY_SCROLL = 'modal-open'

class NewUserModal extends Element {
    constructor() {
        super()

    }

    attachListeners() {
        const closeBtn = this.modalDiv.querySelector('.close')
        closeBtn.addEventListener('click', () => this.close())
    }

    close() {
        this.backdrop.remove()
        document.body.classList.remove(BODY_SCROLL)
        this.modalDiv.remove()
    }

    renderBackdrop() {
        this.backdrop = this.createElement('div', ['modal-backdrop', 'fade'])
        document.body.append(this.backdrop)
    }

    animateShow() {
        requestAnimationFrame(() => {
            this.backdrop.classList.add('show')
            this.modalDiv.classList.add('show')
        })
    }

    render() {
        this.modalDiv = this.createElement('div', ['modal', 'fade'])
        this.modalDiv.style.display = 'block'
        document.body.classList.add(BODY_SCROLL)
        this.modalDiv.innerHTML = `
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Please enter your credentials</h5>
                        <button type="button" class="close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        
                    </div>
                </div>
            </div>
        `
        const newForm = new NewUserForm((user) => {
            const tasksBoard = document.querySelector(".tasks-board");
            const { name, username, email, company: { name: companyname } = {}, id } = user;
            const cardUser = new UserCard(name, username, email, companyname, id)
            tasksBoard.append(cardUser.render());
            console.log(user);
            this.close();
        });
        this.modalDiv.querySelector('.modal-body').append(newForm.render());
        this.renderBackdrop();
        this.attachListeners();
        // document.body.append(this.modalDiv);
        this.animateShow()
        return this.modalDiv
    }
}

export default NewUserModal