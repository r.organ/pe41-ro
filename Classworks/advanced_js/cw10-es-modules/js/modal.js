import Element from './element.js'
import NewUserForm from './new-user-form.js';
import UserCard from './user-card.js';

const BODY_SCROLL = 'modal-open'

class Modal extends Element {
    constructor({headerTitle, body, closeOutside = false}) {
        super()
        this.headerTitle = headerTitle
        this.body = body
        // this.shown = false
        // this.closeOutside = closeOutside
    }

    attachListeners() {
        const closeBtn = this.modalDiv.querySelector('.close')
        closeBtn.addEventListener('click', () => this.close())
        // if (this.closeOutside) {
        //     document.body.addEventListener('click', () => {
        //         const isInsideModal = document.body.closest('.modal-dialog')
        //         console.log(isInsideModal);
        //         if (!isInsideModal) {
        //             this.close()
        //         }
        //     })
        // }
    }

    close() {
        this.backdrop.remove()
        document.body.classList.remove(BODY_SCROLL)
        this.modalDiv.remove()
    }

    renderBackdrop() {
        this.backdrop = this.createElement('div', ['modal-backdrop', 'fade'])
        document.body.append(this.backdrop)
    }

    animateShow() {
        requestAnimationFrame(() => {
            this.backdrop.classList.add('show')
            this.modalDiv.classList.add('show')
        })
    }

    render() {
        this.modalDiv = this.createElement('div', ['modal', 'fade'])
        this.modalDiv.style.display = 'block'
        document.body.classList.add(BODY_SCROLL)
        this.modalDiv.innerHTML = `
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">${this.headerTitle}</h5>
                        <button type="button" class="close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        
                    </div>
                </div>
            </div>
        `

        if (this.body) {
            this.modalDiv.querySelector('.modal-body').append(this.body);
        }
        this.renderBackdrop();
        this.attachListeners();
        // document.body.append(this.modalDiv);
        this.animateShow()
        return this.modalDiv
    }
}

export default Modal
