import Element from './element.js'
import Modal from './modal.js'
import NewTodoForm from './new-todo-form.js'

class UserCard extends Element {
    constructor(name, username, email, company, id) {
        super()
        this.name = name;
        this.username = username;
        this.email = email;
        this.company = company;
        this.id = id;
    }

    renderHeader() {
        const header = this.createElement("div", ["card-header"])
        header.insertAdjacentHTML("afterbegin", `<div><a href="mailto:${this.email}">${this.name}</a><br><span>(@${this.username})</span><div>`);
        header.insertAdjacentHTML('afterbegin', '<span class="icon-del"><svg fill="#000000" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" width="24px" height="24px"><path d="M 10.806641 2 C 10.289641 2 9.7956875 2.2043125 9.4296875 2.5703125 L 9 3 L 4 3 A 1.0001 1.0001 0 1 0 4 5 L 20 5 A 1.0001 1.0001 0 1 0 20 3 L 15 3 L 14.570312 2.5703125 C 14.205312 2.2043125 13.710359 2 13.193359 2 L 10.806641 2 z M 4.3652344 7 L 5.8925781 20.263672 C 6.0245781 21.253672 6.877 22 7.875 22 L 16.123047 22 C 17.121047 22 17.974422 21.254859 18.107422 20.255859 L 19.634766 7 L 4.3652344 7 z"/></svg></span>')
        header.insertAdjacentHTML('beforeend', `<svg class="icon-add-task" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-plus" viewBox="0 0 16 16">
        <path d="M8 7a.5.5 0 0 1 .5.5V9H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V10H6a.5.5 0 0 1 0-1h1.5V7.5A.5.5 0 0 1 8 7z"/>
        <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
      </svg>`)
        header.addEventListener('click', (e) => {
            const delTarget = e.target.closest('.icon-del');
            if (delTarget) {
                const toDelUser = confirm('Delete User?');
                if (toDelUser) {
                    this.userCard.remove();
                    fetch(`https://ajax.test-danit.com/api/json/users/${this.id}`, {
                        method: 'DELETE'
                    })
                }
            }
            const addTarget = e.target.closest('.icon-add-task');
            if (addTarget) {
                const newTodoForm = new NewTodoForm()
                const newTaskModal = new Modal({
                    headerTitle: 'Assign new task',
                    body: newTodoForm.render(),
                    closeOutside: true
                });
                document.body.insertAdjacentElement('beforeend', newTaskModal.render())
            }
        })
        return header
    }
    renderBody() {
        const body = this.createElement("div", ["card-body"]);
        this.tasksContainer = this.createElement("ul", ["list-group", "task-list"]);
        body.append(this.tasksContainer);

        this.tasksContainer.addEventListener('click', (event) => {
            const liTarget = event.target.closest('.task-list--item');
            const delTraget = event.target.closest('.icon-del');
            if (delTraget) {
                const toDelete = confirm('Remove task?');
                if (toDelete) {
                    fetch(`https://ajax.test-danit.com/api/json/todos/${liTarget.dataset.id}`, {
                        method: 'DELETE'
                    })
                    delTraget.closest('.task-list--item').remove();
                }
                return
            }
            if (liTarget) {
                const isCompleted = liTarget.classList.contains("task-list--item-completed")
                liTarget.classList.toggle("task-list--item-completed")
                fetch(`https://ajax.test-danit.com/api/json/todos/${liTarget.dataset.id}`, {
                    method: 'PATCH',
                    body: JSON.stringify({
                        completed: !isCompleted
                    })
                })
            }

        })

        return body
    }
    renderTasks(todos) {
        todos.forEach((todo, index) => {
            if (index <= 3) {
                this.displayTodos(todo);
            }

        })
        if (todos.length >= 4) {
            const btnMore = this.createElement('button', ['btn', 'btn-info'], 'Show More')
            this.tasksContainer.after(btnMore)
            btnMore.addEventListener('click', () => {
                todos.forEach((todo, index) => {
                    if (index > 3) {
                        this.displayTodos(todo);
                    }
                })
                btnMore.remove();
            })
        }
    }

    displayTodos(todo) {
        const li = this.createElement("li", ["list-group-item", "task-list--item", "list-group-item-action"]);
        li.dataset.id = todo.id;
        li.insertAdjacentHTML('afterbegin', '<span class="icon-del"><svg fill="#000000" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" width="24px" height="24px"><path d="M 10.806641 2 C 10.289641 2 9.7956875 2.2043125 9.4296875 2.5703125 L 9 3 L 4 3 A 1.0001 1.0001 0 1 0 4 5 L 20 5 A 1.0001 1.0001 0 1 0 20 3 L 15 3 L 14.570312 2.5703125 C 14.205312 2.2043125 13.710359 2 13.193359 2 L 10.806641 2 z M 4.3652344 7 L 5.8925781 20.263672 C 6.0245781 21.253672 6.877 22 7.875 22 L 16.123047 22 C 17.121047 22 17.974422 21.254859 18.107422 20.255859 L 19.634766 7 L 4.3652344 7 z"/></svg></span>')
        if (todo.completed) {
            li.classList.add("task-list--item-completed");
        }
        const spanForLi = this.createElement("span", '', todo.title);
        li.append(spanForLi);
        this.tasksContainer.append(li);

    }

    renderFooter() {
        const footer = this.createElement("div", ["card-footer"]);
        if (this.company) {
            footer.insertAdjacentHTML("afterbegin", `<small class="text-muted">${this.company}</small>`)
        }
        return footer
    }
    render() {
        this.userCard = this.createElement("div", ["card", "user-card"])
        this.userCard.append(this.renderHeader(), this.renderBody(), this.renderFooter());
        return this.userCard;
    }
}

export default UserCard