// присвоїти в змінні назву продукту, модель та ціну та
// вивести в консоль значення кожної створеної змінної

const product = {
    name: 'iPhone',
    model: '12 Pro',
    company: 'Apple',
    price: 1400
}
const {name: iphoneName, model, company, price, hasNFC = true} = product;

console.log(iphoneName);
console.log(model);
console.log(company);
console.log(price);
console.log(hasNFC);