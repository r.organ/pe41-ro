// - створити об'єкт вакансії для фронтенду (frontendVacancy) та бекенду (backendVacancy), які будуть мати наступний вигляд
// {
//    company: 'DAN.IT',
//    city: 'Kyiv',
//    skills: [...]
// }
// - вивести кожну вакансію в консоль

const address = {
    name: 'Kyiv',
    type: 'city',
    country: 'Ukraine'
}

const company = {
    name: 'DAN.IT',
    employeesCount: 32,
    type: 'private'
}

const skills = {
    frontend: [
        {
            name: "JS",
            experience: 3
        },
        {
            name: "React + Redux",
            experience: 2
        },
        {
            name: "HTML5",
            experience: 2
        },
        {
            name: "CSS3",
            experience: 2
        },
    ],
    backend: [
        {
            name: 'PHP',
            experience: 2
        }
    ]
}

const {name: addressName} = address;
const {name: companyName} = company;

const {frontend, backend} = skills;

console.log(frontend)

const frontendVacancy = {
    company: companyName,
    city: addressName,
    skills: frontend
}
const backendVacancy = {
    company: companyName,
    city: addressName,
    skills: backend
}

console.log(frontendVacancy);
console.log(backendVacancy);
