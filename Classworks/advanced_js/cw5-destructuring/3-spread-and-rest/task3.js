// - написати функцію getCountryByCode, яка буде знаходити країну по коду та
// повертати всі її поля у вигляді об'єкту крім поля code
//
// - написати функцію getCapitalByName, яка буде знаходити країну по коду та
// повертати назву столиці та назву країни в форматі { city: 'Kyiv', country: 'Ukraine' }
//
// - написати функцію getContinentByCode, яка буде повертати назву континенту по коду країни
// наприклад getContinentByCode('US') повинна повертати 'North America'
//
// - написати функцію getBiggestCountry, яка буде повертати ім'я та площу найбільшої країни у
// вигляді об'єкту

// ADVANCED
// - написати функцію getContinentData, яка буде повертати суму всіх площ країн вказаного континенту та список
// країн цього континенту у вигляді об'єкту { area: 10_324_532, countries: ['China', 'India'] }


const CONTINENTS = {
    NA: 'North America',
    EU: 'Europe',
    AS: 'Asia'
}

const COUNTRIES = [{
    code: 'US',
    name: 'United States',
    capital: 'Washington',
    area: 9629091,
    continent: 'NA'
},
    {
        code: 'DE',
        name: 'Germany',
        capital: 'Berlin',
        area: 3570210,
        continent: 'EU'
    },
    {
        code: 'DK',
        name: 'Denmark',
        capital: 'Copenhagen',
        area: 430940,
        continent: 'EU'
    },
    {
        code: 'UA',
        name: 'Ukraine',
        capital: 'Kyiv',
        area: 603700,
        continent: 'EU'
    },
    {
        code: 'CN',
        name: 'China',
        capital: 'Beijing',
        area: 9596960,
        continent: 'AS'
    },
    {
        code: 'GB',
        name: 'United Kingdom',
        capital: 'London',
        area: 244820,
        continent: 'EU'
    },
    {
        code: 'IN',
        name: 'India',
        capital: 'New Delhi',
        area: 3287590,
        continent: 'AS'
    }
];

// function getCountryByCode(code, countries) {
//     let otherFields = {};
//     countries.forEach(country => {
//         if (country.code === code) {
//             const { code, ...other } = country;
//             otherFields = other;
//         }
//     });
//     return otherFields;
// }

function getCountryByCode(countryCode, countries) {
    const country = countries.find((country => country.code === countryCode));
    const { code, ...other } = country;
    return other;
}

function getCapitalByName(countryCode, countries) {
    const countryFull = countries.find((country => country.code === countryCode));
    const { capital: city, name: country } = countryFull;
    // return { city: city, country: country };
    return { city, country };
}


function getContinentByCode(countryCode, countries, continents) {
    const countryFull = countries.find((country => country.code === countryCode));
    const { continent: shortContinent } = countryFull;
    return continents[shortContinent];
}

function getBiggestCountry(countries) {
    let [currentCountry] = countries;
    countries.forEach(country => {
        if (country.area > currentCountry.area) {
            currentCountry = country;
        }
    })
    const { name, area } = currentCountry;
    return { name, area };
}

function getContinentData(shortContinent, countries) {
    let fullArea = 0;
    let countyList = [];
    countries.filter(country => country.continent === shortContinent)
        .forEach(country => {
            fullArea += country.area;
            countyList.push(country.name)
        })
    return { fullArea, countyList };
}

console.log(getContinentData('AS', COUNTRIES));

