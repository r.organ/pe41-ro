// - присвоїти у змінні ім'я, прізвище, пошту та дату народження
// - вивести в консоль ім'я, прізвище, пошту та РІК народження

const person = 'Tom, Myers, tom.myers@example.com, 8/7/1980';

const persontoArr = person.split(', ')
console.log(persontoArr);

const [name, surname, email, dateofBirth] = persontoArr;
console.log(name);
console.log(surname);
console.log(email);
console.log(dateofBirth);

const dateYear = new Date(dateofBirth).getFullYear()
console.log(dateYear)