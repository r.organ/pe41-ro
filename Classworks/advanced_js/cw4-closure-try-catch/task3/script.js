/**
 * Рахувати кількість натискань на пробіл, ентер,
 * шифт та альт клавіші
 * Відображати результат на сторінці
 *
 * Створити функцію, яка приймає тільки
 * назву клавіші, натискання якої потрібно рахувати,
 * а сам лічильник знаходиться в замиканні цієї функції
 * (https://learn.javascript.ru/closure)
 * id елемента, куди відображати результат має назву
 * "KEY-counter"
 *
 *
 */

// let counterEnter = 0;
// let counterSpace = 0;
// let counterShift = 0;

function createCounter(keyName){
    let counter = 0;
    const element = document.createElement('p');
    element.innerText = `${keyName} pressed: 0`
    document.body.append(element)
    return(event) => {
        if (event.code === keyName) {
            counter = counter + 1;
            element.innerText = `${keyName} pressed ${counter}`
    //         let targetEl = document.querySelector(id);
    //         counter = counter + 1;
    //         targetEl.textContent = counter;
     }
        }
}

const increaseEnter = createCounter('Enter',);
const increaseSpace = createCounter('Space',);
const increaseShiftLeft = createCounter('ShiftLeft',);

window.addEventListener('keyup', function (event) {
    increaseEnter(event);
    increaseSpace(event);
    increaseShiftLeft(event);

    // if (event.code === "Enter") {
    //     let enterCounter = document.querySelector('#enter-counter');
    //     counterEnter = counterEnter + 1;
    //     enterCounter.textContent = counterEnter;
    // } else if (event.code === "Space") {
    //     let spaceCounter = document.querySelector('#space-counter');
    //     counterSpace = counterSpace + 1;
    //     spaceCounter.textContent = counterSpace;
    // } else if (event.key === "Shift") {
    //     let shiftCounter = document.querySelector('#shift-counter');
    //     counterShift = counterShift + 1;
    //     shiftCounter.textContent = counterShift;
    // }

})
