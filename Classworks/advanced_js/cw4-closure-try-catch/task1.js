/**
 * Створити лічильники кількості населення для 3х
 * країн: України, Німеччини та Польщі
 * Для всіх країн населення спочатку дорівнює 0
 *
 * Збільшити населення на 1_000_000 для
 * України - двічі
 * Німеччини - чотири рази
 * Польщі - один раз
 *
 * При кожному збільшенні виводити в консоль
 * 'Населення COUNTRY_NAME збільшилося на X і становить Y'
 *
 * ADVANCED:
 * - Кожну секунду збільшувати населення України на 100_000 та також
 * виводити в консоль 'Населення COUNTRY_NAME збільшилося на X і становить Y'
 *
 */

function countPopulation(country) {
    let population = 0;

    return (value = 1_000_000) => {
        population += value;
        console.log(`Населення ${country} збільшилось на ${value} і становить ${population} `)
    }
}

const increaseUkrainePopulation = countPopulation('Ukraine');
increaseUkrainePopulation()
increaseUkrainePopulation()

const increaseGermanyPopulation = countPopulation('Germany');
increaseGermanyPopulation();
increaseGermanyPopulation();
increaseGermanyPopulation();
increaseGermanyPopulation();

setInterval(() => {
    increaseGermanyPopulation(100_000)
}, 1000)