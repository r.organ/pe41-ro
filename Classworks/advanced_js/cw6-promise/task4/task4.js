/** Написати функціонал логіну на сайт

при натисканні на кнопку Login перевіряються введені дані
через 1 секунду, якщо дані валідні, то виводиться модальне вікно з текстом Success
інакше в блок помилок на сторінці виводиться повідомлення Invalid data



Технічні вимоги

При натисканні на Login кнопку передається функції authenticate ім'я користувача та пароль
Через вказаний проміжок часу резолвиться або реджектиться проміс відповідно без або з помилкою
Використати наявний масив валідних даних для перевірки **/

const USERS = [
    ['admin', '123'],
    ['john', 'qwe'],
    ['jack', 'asd'],
    ['marry', '123']
]

const authenticate = (username, password) => {
    return new Promise((resolve, reject) =>{
        setTimeout(()=> {
            // const user = USERS.find(user => user[0] === username && user[1] === password)
            const user = USERS.find(([user, pass]) => user === username && pass === password)
            if (user) {
                resolve('Success');
            } else {
                reject('Invalid data')
            }
        }, 2000)
    })
}

const inputUsername = document.querySelector('input[name=username]')
const inputPassword = document.querySelector('input[name=password]')
const form = document.querySelector('#login-form')
const errors = document.querySelector('.errors')
const button = document.querySelector('button')

form.addEventListener('submit',(event)=> {
    errors.innerText = "";
    button.disabled = true;
    event.preventDefault()
    authenticate(inputUsername.value, inputPassword.value)
        .then((success) => {
            alert(success)
            button.disabled = false;
        })
        .catch((e)=> {
            errors.innerText = `${e}`;
            button.disabled = false;
        })
        .finally(() =>  button.disabled = false)
})
