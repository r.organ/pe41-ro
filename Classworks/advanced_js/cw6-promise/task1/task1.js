/**
 * productsPromise робить запит на сервер і іноді з'являється помилка
 *
 * Якщо продукти приходять, то вивести їх списком на сторінку,
 * інакше вивести модальне вікно з ім'ям помилки
 *
 */

const productsPromise = new Promise((resolve, reject) => {
    setTimeout(() => {
        if (Math.random() < 0.5) {
            resolve(['orange', 'apple', 'pineapple', 'melon'])
        } else {
            reject(new Error('Server Error'))
        }
    }, 1000);
});

productsPromise
    .then((fruits)=>{
    const fullList = document.createElement('ul');
    document.body.insertAdjacentElement('afterbegin', fullList)
    fruits.forEach((fruit => {
        const fruitItem = `<li>${fruit}</li>`;
        fullList.insertAdjacentHTML('beforeend', fruitItem)
    }));
})
    .catch((error) => {alert(error.message)})