/**
 * Написати функцію getWeather, яка приймає ім'я міста та
 * повертає проміс, який через 1 секунду зарезолвиться,
 * якщо ми передали Kyiv, Lviv, Kharkiv,
 * інакше буде помилка з текстом 'Невідоме місто'
 *
 */

const getWeather = (city) => {
    return new Promise ((resolve, reject) =>{
        setTimeout(()=> {
            if (city === 'Kyiv') {
                resolve ('Sunny')
            } else if (city === 'Lviv' || city === 'Kharkiv'){
                resolve ('Cloudy')
            } else {
                reject(new Error ('Невідоме місто'))
            }
        }, 1000)
    })
}

const city = prompt('Enter City:')
getWeather(city).then( (wether)=> alert (`In ${city} the wether is ${wether}`)).catch((error) =>{alert(error.message)})
