const user = document.querySelector('.user');
const form = document.querySelector('.form-inline');

const getUser = async id => {
    const response = await fetch(`https://ajax.test-danit.com/api/json/users/${id}`)
    const data = await response.json()
    return data
}

class Element {
    createElement(tag = 'div', text) {
        const element = document.createElement(tag)
        element.textContent = text
        return element
    }
}

class Link extends Element {
    constructor({href, text}) {
        super()
        this.href = href;
        this.text = text;
    }

    render() {
        const a = this.createElement('a', this.text)
        a.href = this.href;
        return a
    }
}

class Paragraph extends Element {
    constructor({text}) {
        super()
        this.text = text;
    }

    render() {
        return this.createElement('p', this.text)
    }
}

form.addEventListener('submit', async (event) => {
    event.preventDefault();
    const input = document.querySelector('.form-control');
    const value = input.value;
    user.innerHTML = null;

    const userIds = value.split(',')
    console.log(userIds);
    const userPromises = userIds.map(id => getUser(id))

    console.log(userPromises);

    // Promise all
    /* Promise.all(userPromises)
      .then(values => {
        console.log(values);
        values.forEach(data => {
          const p = new Paragraph({ text: data.name })
          const emailEl = new Link({text: data.email, href: data.email})
          const websiteEl = new Link({text: data.email, href: data.email})
          user.append(
            p.render(),
            emailEl.render(),
            websiteEl.render()
          )
        })
      }) */

    for await (const data of userPromises) {
        const p = new Paragraph({ text: data.name })
        const emailEl = new Link({text: data.email, href: data.email})
        const websiteEl = new Link({text: data.email, href: data.email})
        user.append(
            p.render(),
            emailEl.render(),
            websiteEl.render()
        )
    }
});
