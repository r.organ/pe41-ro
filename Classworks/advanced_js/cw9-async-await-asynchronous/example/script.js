console.log(0);
console.log(1);

setTimeout(() => console.log(2), 20);

Promise.resolve(3)
    .then(num => console.log(num));

const promise = new Promise(function(resolve, reject) {
    resolve(4)
})
promise.then(num => console.log(num));

console.log(5);

setTimeout(() => console.log(6), 0);

console.log(7);

// 0, 1, 5, 7, 6, 3, 4, 2
