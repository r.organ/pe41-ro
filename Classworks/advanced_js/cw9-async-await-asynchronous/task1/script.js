/*
Дати користувачу можливість вводити id альбомів, які він хоче отримати, та виводити результат в таблицю
У таблиці також можна обрати декілька альбомів та видалити їх при натисканні на Delete
У таблиці відображається інформація про власника альбома

Технічні вимоги

Користувач може вводити id в форматі "1, 3, 5", "2-4" або змішано "1, 4, 5-10, 2"
Запит на одного юзера відбувається тільки один раз
Вся таблиця відображається за допомогою js
При повторному запиті таблиця оновлюється новими даними
При натисканні на Delete відбувається видалення обраних альбомів


Advanced

Додати чекбокс для можливості включати/виключати чекбокси для всіх альбомів
 */


const form = document.querySelector('.form');

const getAlbum = async (id) => {
    return axios.get(`https://ajax.test-danit.com/api/json/albums/${id}`)
};

const getOwner = async (id) => {
    return axios.get(`https://ajax.test-danit.com/api/json/users/${id}`)
};

const removeAlbum = async (id) => {
    return axios.delete(`https://ajax.test-danit.com/api/json/albums/${id}`)
};

class AlbumManager {
    constructor() {
        this.containter = document.createElement('div');
        this.containter.classList.add('container', 'mt-5');
        this.containter.innerHTML = `<button id="del" class="btn btn-danger">Delete</button>
   <table class="table">
       <thead>
         <tr>
           <th scope="col">
               <!-- <div class="input-group-prepend">
                   <div class="input-group-text">
                       <input type="checkbox" aria-label="Radio button for following text input">
                   </div>
               </div> -->
           </th>
           <th scope="col">id</th>
           <th scope="col">Title</th>
           <th scope="col">Owner</th>
         </tr>
       </thead>
       <tbody>
         <tr>
           <td>
               <div class="input-group-prepend">
                   <div class="input-group-text">
                       <input type="checkbox" aria-label="Radio button for following text input">
                   </div>
               </div>
           </td>
           <th scope="row">1</th>
           <td>Memories</td>
           <td>
               <div>Ervin Howell</div>
               <div>
                   <a href="mailto:useremail@gmail.com"">@mdo</a>
               </div>
           </td>
           <td></td>
         </tr>
       </tbody>
     </table>`;
        this.tbody = this.containter.querySelector('tbody');
        this.deleteButton();
    }

    render() {
        return this.containter;
    }

    add(album, owner) {
        const tr = document.createElement('tr');
        tr.setAttribute('data-id', album.id);
        tr.innerHTML = `<td>
        <div class="input-group-prepend">
            <div class="input-group-text">
                <input type="checkbox" value="${album.id}" aria-label="Radio button for following text input">
            </div>
        </div>
    </td>
    <th scope="row">${album.id}</th>
    <td>${album.title}</td>
    <td>
        <div>${owner.name}</div>
        <div>
            <a href="mailto:${owner.email}"">@${owner.username}</a>
        </div>
    </td>
    <td></td>`;
        this.tbody.append(tr);
    }
    clear() {
        this.tbody.innerText = null;
    }

    remove(id) {
        const tr = this.containter.querySelector(`tr[data-id="${id}"]`);
        tr.remove();
    }

    deleteButton() {
        const deleteButton = this.containter.querySelector('#del');
        deleteButton.addEventListener('click', async () => {
            const inputCheckboxes = this.containter.querySelectorAll('input[type=checkbox]');
            // for (const inputCheckbox of inputCheckboxes) {
            //   if (inputCheckbox.checked === true) {
            //     await removeAlbum(inputCheckbox.value);
            //     this.remove(inputCheckbox.value);
            //   }
            // }
            const filterCheckboxes = [...inputCheckboxes].filter((inputCheckbox) => {
                return inputCheckbox.checked;
            });
            const checkedIds = filterCheckboxes.map((checkbox) => {
                return checkbox.value;
            });
            const checkedPromises = checkedIds.map((id) => {
                return removeAlbum(id);
            });
            Promise.all(checkedPromises).then(() => {
                checkedIds.forEach((id) => this.remove(id));
            });
        });
    }
}

const manager = new AlbumManager();
document.body.append(manager.render());

form.addEventListener('submit', async (event) => {
    event.preventDefault();
    manager.clear();
    const input = document.querySelector('.form-control');
    const albumIds = input.value.split(',');
    try {
        const albumPromises = albumIds.map((albumId) => {
            return getAlbum(albumId);
        });

        for await (const result of albumPromises) {
            console.log(result.data);
            const owner = await getOwner(result.data.userId);
            manager.add(result.data, owner.data);
        }
    } catch(err) {
        alert('Error')
    }
});
