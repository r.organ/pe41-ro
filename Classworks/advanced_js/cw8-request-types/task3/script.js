/*
Реалізувати функціонал форми для створення нових користувачів

Технічні вимоги

при натисканні на кнопку форми відправляти POST запит з введеною інформацією
при успішному виконанню запиту показувати модальне вікно (alert), що користувач створений
 */

const form = document.querySelector('.form');
const userName = document.querySelector('#name');
const userEmail = document.querySelector('#email');
const userWebsite = document.querySelector('#website');


form.addEventListener('submit', (event) => {
    event.preventDefault()
    const nameValue = userName.value;
    const emailValue = userEmail.value;
    const websiteValue = userWebsite.value;
    fetch(`https://ajax.test-danit.com/api/json/users`, {
        method: 'POST',
        body: JSON.stringify({name: nameValue, email: emailValue, website: websiteValue})
    })

        .then(response => response.json())
        .then(data => alert('Користувач створений'))


})
