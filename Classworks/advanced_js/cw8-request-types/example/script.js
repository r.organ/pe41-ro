/* fetch('https://ajax.test-danit.com/api/json/albums?userId=3', {
    method: 'GET'
})
    .then(response => response.json())
    .then(data => console.log(data)) */

// POST
/* fetch('https://ajax.test-danit.com/api/json/albums', {
    method: 'POST',
    body: JSON.stringify({title: 'New Album 123'})
})
    .then(response => response.json())
    .then(data => console.log(data)) */

fetch('https://ajax.test-danit.com/api/json/albums/3', {
    method: 'PUT',
    body: JSON.stringify({title: 'New Album 123'})
})
    .then(response => {
        if (response.ok) {
            return response.json()
        } else {
            throw new Error('error')
        }
    })
    .then(data => console.log(data))
    .catch(() => alert('Sorry, error'))


/* fetch('https://ajax.test-danit.com/api/json/albums/100', {
    method: 'DELETE'
})
    .then(response => {
        if (!response.ok) {
            throw new Error('error deleting')
        }
    })
    .then(() => console.log('deleted'))
    .catch(() => alert('error'))
 */
