const formElem = document.querySelector('form');
const selectPostElem = document.getElementById('post');
const titleElem = document.getElementById('title');
const bodyElem = document.getElementById('body');

const request = ({url, data, options = {}}) => {
    return fetch(url, {
        method: 'GET',
        ...options,
        body: JSON.stringify(data)
    })
        .then(response => {
            if (response.ok) {
                return response.json()
            } else {
                throw new Error({message: 'error'})
            }
        })
}

selectPostElem.addEventListener('change', () => {
    const id = selectPostElem.value;
    request({url: `https://ajax.test-danit.com/api/json/posts/${id}`})
        .then(result => {
            console.log(result);
            let { title, body } = result;
            titleElem.value = title;
            bodyElem.value = body;
        })
})

formElem.addEventListener('submit', (e) => {
    e.preventDefault();
    const titleValueUpdated = titleElem.value;
    const bodyValueUpdated = bodyElem.value;
    const id = selectPostElem.value;
    request({
        url: `https://ajax.test-danit.com/api/json/posts/${id}`,
        options: { method: 'PATCH' },
        data: { title: titleValueUpdated, body: bodyValueUpdated }
    })
})
