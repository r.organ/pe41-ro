/*
Розробити функціонал для редагування наявних постів

Технічні вимоги

при зміні значення в селекті отримуємо з сервера наявний пост по ID
отримані дані (title та body) відображаємо в полях вводу
при натисканні на кнопку Оновлення відправляти PATCH запит по вибраному ID поста

Advanced

при завантаженні сторінки робити запит на весь список постів та відображати існуючі варіанти в селекті
 */



const formElem = document.querySelector('form');
const selectPostElem = document.getElementById('post');
const titleElem = document.getElementById('title');
const bodyElem = document.getElementById('body');

selectPostElem.addEventListener('change', () => {
    const id = selectPostElem.value;
    fetch(`https://ajax.test-danit.com/api/json/posts/${id}/`)
        .then((response) => response.json())
        .then((result) => {
            console.log(result);
            let { title, body } = result;
            titleElem.value = title;
            bodyElem.value = body;
        })
})

formElem.addEventListener('submit', (e) => {
    e.preventDefault();
    const titleValueUpdated = titleElem.value;
    const bodyValueUpdated = bodyElem.value;
    const id = selectPostElem.value;
    fetch(`https://ajax.test-danit.com/api/json/posts/${id}/`, {
        method: 'PATCH',
        body: JSON.stringify({ title: titleValueUpdated, body: bodyValueUpdated })
    })
        .then((response) => response.json())

})
