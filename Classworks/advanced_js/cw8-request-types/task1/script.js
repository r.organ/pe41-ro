const user = document.querySelector('.user');
const form = document.querySelector('.form-inline');

form.addEventListener('submit', (event) => {
    event.preventDefault();
    const input = document.querySelector('.form-control');
    const value = input.value;
    fetch(`https://ajax.test-danit.com/api/json/users/${value}`)
        .then((response) => response.json())
        .then((data) => {
            user.innerText = '';
            console.log(data);
            const userName = document.createElement('p');
            userName.innerText = data.name;
            const userEmail = document.createElement('a');
            userEmail.href = data.email;
            userEmail.innerText = data.email;
            const userWebsite = document.createElement('a');
            userWebsite.href = data.website;
            userWebsite.innerText = data.website;
            user.append(userName, userEmail, userWebsite);
        });
});
