/*
Додати функціонал пошуку інформації про користувачів за ID,
використовуючи наявне АПІ https://ajax.test-danit.com/api-pages/jsonplaceholder.html

Технічні вимоги

робити запит для отримання інформації тільки по одному користувачу
відображати ім'я у вигляді текстового блоку, а електронну пошту та вебсайт у вигляді посилання
 */

const user = document.querySelector('.user');
const form = document.querySelector('.form-inline');

const getUser = id => {
    return fetch(`https://ajax.test-danit.com/api/json/users/${id}`)
        .then((response) => response.json())
}

class Element {
    createElement(tag = 'div', text) {
        const element = document.createElement(tag)
        element.textContent = text
        return element
    }
}

class Link extends Element {
    constructor({href, text}) {
        super()
        this.href = href;
        this.text = text;
    }

    render() {
        const a = this.createElement('a', this.text)
        a.href = this.href;
        return a
    }
}

class Paragraph extends Element {
    constructor({text}) {
        super()
        this.text = text;
    }

    render() {
        return this.createElement('p', this.text)
    }
}

form.addEventListener('submit', (event) => {
    event.preventDefault();
    const input = document.querySelector('.form-control');
    const value = input.value;
    user.innerHTML = null;
    getUser(value)
        .then((data) => {
            const p = new Paragraph({ text: data.name })
            const emailEl = new Link({text: data.email, href: data.email})
            const websiteEl = new Link({text: data.email, href: data.email})
            user.append(
                p.render(),
                emailEl.render(),
                websiteEl.render()
            )
        });
});
