class Card {
    constructor(name, email, postId, title, body) {
        this.name = name;
        this.email = email;
        this.postId = postId;
        this.title = title;
        this.body = body;
    }

    createElement(elemType, classNames) {
        const element = document.createElement(elemType);
        element.classList.add(...classNames);
        return element
    }

    render() {
        this.card = this.createElement('div', ['card-wrapper']);
        this.card.innerHTML = `
        <div class = "header-wrapper">
             <div class = "name-wrapper">
                <p class = "name">${this.name}</p>
                <a class = "email" href=mailto:${this.email}>@${this.email}</a>
            </div>
        </div>
        <button class="btn btn-light">x</button>
        <p class = "post-title">${this.title}</p>
        <p class = "post-body">${this.body}</p>
        `
        this.deleteBtn()
        return this.card
    }

    deleteBtn() {
        this.card.addEventListener('click', (e) => {
            const delTarget = e.target.closest('.btn');
            if (delTarget) {
                const toDelCard = confirm('Are you sure you want to delete this card?');
                if (toDelCard) {
                    this.card.remove();
                    fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
                        method: 'DELETE'
                    })
                }
            }
        })
    }
}

async function displayCard() {
    const root = document.querySelector('#root')
    const preloader = document.querySelector('.text-center')
    const response = await fetch('https://ajax.test-danit.com/api/json/users')
    const users = await response.json()
    users.forEach(async user => {
        const {name, username, id} = user;
        const response = await fetch('https://ajax.test-danit.com/api/json/posts')
        const posts = await response.json()
        preloader.style.display = 'none';
        const userPosts = posts.filter(post => post.userId === id)
        userPosts.forEach(post => {
            const {id, title, body} = post;
            const card = new Card(name, username, id, title, body)
            root.append(card.render())
        })
    })

}

displayCard()
