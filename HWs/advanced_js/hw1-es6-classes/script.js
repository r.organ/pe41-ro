class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super (name, age, salary)
        this.lang = lang;
    }
    get salary() {
        return `${this.name} salary is ${this._salary * 3} USD`
    }
}

const johnDev = new Programmer('John', 25, 1500, 'JS')
const rebeccaDev = new Programmer ('Rebecca', 27, 1200, 'Python')
const mikeDev = new Programmer('Mike', 23, 2000, 'Scala')

console.log(johnDev)
console.log(rebeccaDev)
console.log(mikeDev)

console.log(johnDev.salary)
console.log(rebeccaDev.salary)
console.log(mikeDev.salary)