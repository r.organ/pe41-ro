class Movie {

    constructor(episodeId, characters, name, openingCrawl) {
        this.episodeId = episodeId;
        this.characters = characters;
        this.name = name;
        this.openingCrawl = openingCrawl;
    }

    createElement(elemType, classNames) {
        const element = document.createElement(elemType);
        element.classList.add(...classNames);
        return element
    }

    render() {
        this.filmElem = this.createElement('div', ["div"]);
        this.filmElem.innerHTML = `
            <h4 class = "film-title">Episode ${this.episodeId}: ${this.name}</h4>
            <p class = 'film-desc-title'>Description:</p>
            <p class = 'film-desc'>${this.openingCrawl}</p>
            <p class = 'film-desc-title'>PERSONS AND TEAMS:</p>
            `
        this.filmElem.append(this.renderActors())

        return this.filmElem
    }

    renderActors() {
        this.preloader = this.createElement('div', ['preloader'])
        this.preloader.innerHTML = `
            <div class="dot dot2 d21"></div>
            <div class="dot dot2 d22"></div>
            <div class="dot dot2 d23"></div>`
        this.preloader.style.opacity = '1';

        this.charactersList = this.createElement("ul", []);

        Promise.all(this.characters.map(link => fetch(link)
            .then(response => response.json())))
            .then(data => {
                this.preloader.style.opacity = '0';
               data.map(value => {
                    const {name} = value;
                    this.charactersList.innerHTML += `<li>${name}</li>`
                })
            });
        this.filmElem.append(this.preloader)
        return this.charactersList
    }
}

function displayMovies() {
    const root = document.querySelector('#root');
    fetch('https://ajax.test-danit.com/api/swapi/films')
        .then(response => response.json())
        .then(data => data
            .forEach(film => {
                const {episodeId, characters, name, openingCrawl} = film;
                const movie = new Movie(episodeId, characters, name, openingCrawl)
                root.append(movie.render());
                  }))
}
displayMovies()