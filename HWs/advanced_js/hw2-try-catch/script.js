const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.querySelector("#root")
const bookList = document.createElement('ul');
root.append(bookList)

function createBooksList (book) {
    // option 1
    // const bookListItem = `<li> <b>Book Author:</b> <i>${book.author}</i>, <b>Book Name:</b> <i>${book.name}</i>, <b>Book Price:</b> <i>${book.price}</i></li>`;
    // bookList.innerHTML += bookListItem;

    // option 2
    // const bookListItem = `<li>${Object.values(book)}</li>`;
    // bookList.innerHTML += bookListItem;

    // option 3
    const prop = Object.entries(book).map((value) => value).join('; ').replaceAll(',', ': ')
    const bookItem = `<li>${prop}</li>`
    bookList.innerHTML += bookItem;
}

const bookFields = ['author', 'name', 'price'];

function validateBook(books, fieldsForCheck) {
    books.forEach((book) => {
        try {
            // option one:
            // if (!book.author) {
            //     throw new Error('Author of book is not found');
            // } else if (!book.name) {
            //     throw new Error('Name of book is not found');
            // } else if (!book.price) {
            //     throw new Error('Price of book is not found');
            // }

            // option two
           const emptyProp = fieldsForCheck.find(function (filed){
               return !Object.keys(book).includes(filed)
           })

           if (emptyProp) {
                throw new Error (`The book "${book.name}" (under №${books.indexOf(book) + 1}) doesn't have property "${emptyProp}"`)
            }

             createBooksList(book);

        }
        catch (error) {
            console.log(error);
        }
    })
}
validateBook(books, bookFields);


