const searchBtn  = document.querySelector('.btn')

searchBtn.addEventListener('click', e =>{
    async function getDetailsByIp (url) {
        const response = await fetch(url);
        const ip = await response.json();

        const resp = await fetch(`http://ip-api.com/json/${ip.ip}?fields=continent,country,regionName,city,district`)
        const ipDetails = await resp.json();

        const addressbyIp = document.createElement('div');
        addressbyIp.classList.add('address-wrapper')
        addressbyIp.innerHTML = `
            <h4> Ку-ку, ось ти де: </h4>
            <p>Континет: ${ipDetails.continent}</p>
            <p>Країна: ${ipDetails.country}</p>
            <p>Регіон: ${ipDetails.regionName}</p>
            <p>Місто: ${ipDetails.city}</p>
            <p>Район: ${ipDetails.district = "-"}</p>`

        searchBtn.after(addressbyIp)
    }
    getDetailsByIp('https://api.ipify.org/?format=json')
})