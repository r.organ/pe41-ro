import { Formik, Field, Form, ErrorMessage } from "formik";
import { object, string, number } from "yup";
import { useSelector, useDispatch } from "react-redux";
import NumberFormat from "react-number-format";
import "./purchaseForm.scss";
import { clearBasket } from "../../store/basket/actions";

const NumberField = ({ field }) => {
  return (
    <NumberFormat
      {...field}
      format="+38 (###) ###-##-##"
      allowEmptyFormatting
      mask="_"
    />
  );
};
const purchaseFormSchema = object({
  firstName: string()
    .required("Необхідно вказати ім'я")
    .min(3, "Імʼя занадто коротке"),
  lastName: string()
    .required("Необхідно вказати фамілію")
    .min(3, "Фамілія занадто коротка"),
  age: number().moreThan(15, "Вам має бути більше 16 років"),
  deliveryAddress: string().required("Необхідно вказати адресу доставки"),
  phone: string().required("Необхідно вказати номер телефону"),
  // .length(19, "Недійсний номер"),
});

const PurchaseForm = () => {
  const basket = useSelector((state) => state.basket);

  const dispatch = useDispatch();
  const amountToPay = basket.reduce((acc, amount) => {
    acc += amount.price;
    return acc;
  }, 0);

  return (
    <>
      <Formik
        initialValues={{
          firstName: "",
          lastName: "",
          age: "",
          deliveryAddress: "",
          phone: "",
        }}
        onSubmit={(values) => {
          console.log("Buyer info:", values);
          console.log("Order info:", basket);
          dispatch(clearBasket());
        }}
        validationSchema={purchaseFormSchema}
      >
        {(propsFormik) => {
          return (
            <Form className="form-wrapper">
              <h2 className="form-title"> Ваші дані</h2>
              <div className="field-wrapper">
                <label className="label" htmlFor="firstName">
                  Введіть Ваше Імʼя
                </label>
                <Field id="firstName" name="firstName" />
                <ErrorMessage
                  component="div"
                  name="firstName"
                  className="error"
                />
              </div>
              <div className="field-wrapper">
                <label className="label" htmlFor="lastName">
                  Введіть Ваше Прізвище
                </label>
                <Field id="lastName" name="lastName" />
                <ErrorMessage
                  component="div"
                  name="lastName"
                  className="error"
                />
              </div>

              <div className="field-wrapper">
                <label className="label" htmlFor="age">
                  Введіть Ваш Вік
                </label>
                <Field id="age" name="age" />
                <ErrorMessage name="age" component="div" className="error" />
              </div>
              <div className="field-wrapper">
                <label className="label" htmlFor="deliveryAddress">
                  Введіть Вашу адресу доставки
                </label>
                <Field id="deliveryAddress" name="deliveryAddress" />
                <ErrorMessage
                  name="deliveryAddress"
                  component="div"
                  className="error"
                />
              </div>
              <div className="field-wrapper">
                <label className="label" htmlFor="phone">
                  Введіть Ваш номер телефону
                </label>
                <Field name="phone" component={NumberField} />
                <ErrorMessage name="phone" component="div" className="error" />
              </div>
              <h2 className="total-sum">Загалом до сплати {amountToPay} грн</h2>

              <button type="submit" className="purchase-btn">
                Замовити
              </button>
            </Form>
          );
        }}
      </Formik>
    </>
  );
};

export default PurchaseForm;
