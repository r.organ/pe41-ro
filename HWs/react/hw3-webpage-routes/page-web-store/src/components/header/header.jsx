import Basket from "../icon/basket";
import FavIcon from "../icon/favorite";
import "./header.scss";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const Header = ({ basketCount, favoriteCount }) => {
  const isFavoriteNotEmpty = favoriteCount !== 0;
  const isBasketNotEmpty = basketCount !== 0;
  // console.log(isFavoriteEmpty);

  return (
    <>
      <div className="header-container">
        <Link to="/">
          <h3 className="header-title">Helmets</h3>
        </Link>
        <div className="icon-wrapper">
          <Link to="/basket">
            <div className="cart-container">
              <Basket className="cart-icon" color={"#FF2A47"} />
              {isBasketNotEmpty && (
                <div className="cart-counter">
                  <p>{basketCount}</p>
                </div>
              )}
            </div>
          </Link>
          <Link to="/favorites">
            <div className="favorite-container">
              <FavIcon className="favorite-icon" color={"white"} />
              {isFavoriteNotEmpty && (
                <div className="favorite-counter">
                  <p>{favoriteCount}</p>
                </div>
              )}
            </div>
          </Link>
        </div>
      </div>
    </>
  );
};;;;;;;;;;

Header.propTypes = {
  basketCount: PropTypes.number,
  favoriteCount: PropTypes.number,
};
Header.defaultProps = {
  basketCount: 0,
  favoriteCount: 0,
};

export default Header;
