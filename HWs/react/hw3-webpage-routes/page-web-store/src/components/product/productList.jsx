import ProductCard from "./productCard";
import "./productList.scss";
import PropTypes from "prop-types";

const ProductList = ({
  favoritesToggle,
  list,
  favorites,
  openModal,
  basket,
  removeBtn,
}) => {
  const handleFavorites = (squ) => {
    favoritesToggle(squ);
  };

  return (
    <>
      <div className="list-container">
        {list.map((product) => {
          return (
          <ProductCard
            key={product.squ}
            product={product}
            openModal={openModal}
            handleFavorites={handleFavorites}
            favorites={favorites}
            basket={basket}
            removeBtn={removeBtn}
          />
        );
        })}
      </div>
    </>
  );
};

ProductList.propTypes = {
  list: PropTypes.array.isRequired,
  openModal: PropTypes.func.isRequired,
  favoritesToggle: PropTypes.func.isRequired,
  favorites: PropTypes.array,
};
ProductList.defaultProps = {
  favorites: [],
};

export default ProductList;
