import ProductList from "../components/product/productList";
import { Link } from "react-router-dom";
import "./favorites-page.scss";
import { useEffect, useState } from "react";

const Basket = ({
  list,
  favoritesToggle,
  openModal,
  showModal,
  closeModal,
  favorites,
}) => {
  const [isListEmpty, setIsListEmpty] = useState(true);
  useEffect(() => {
    if (list.length === 0) {
      setIsListEmpty(true);
    } else {
      setIsListEmpty(false);
    }
  }, [list.length]);
  return (
    <>
      <div className="favorites-container">
        <h2 className="favorites-title">Товари у кошику:</h2>
        {isListEmpty ? (
          <div className="favorites-empty-list">
            <h3 className="favorites-empty-list_title">Ваш кошик порожній</h3>
            <img
              className="favorites-empty-list_img"
              src="./img/empty-list.png"
              alt="empty list"
            />
            <p className="favorites-empty-list_text">
              Але ніколи не пізно його поповнити
            </p>
            <Link to="/">
              <button className="favorites-empty-list_btn">На головну</button>
            </Link>
          </div>
        ) : (
          <ProductList
            list={list}
            favoritesToggle={favoritesToggle}
            openModal={openModal}
            showModal={showModal}
            closeModal={closeModal}
            favorites={favorites}
            basket={list}
            removeBtn={true}
          />
        )}
      </div>
    </>
  );
};

export default Basket;
