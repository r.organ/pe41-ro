import "./App.scss";
import { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import getProducts from "./api/getProducts";
import ProductList from "./components/product/productList";
import Header from "./components/header/header";
import Modal from "./components/modal/modal";
import Basket from "./pages/basket";
import Favorites from "./pages/favorites";

const getProductsFromLS = (key) => {
  const lsProducts = localStorage.getItem(key);
  if (!lsProducts) return [];
  try {
    const value = JSON.parse(lsProducts);
    return value;
  } catch (e) {
    return [];
  }
};

const App = () => {
  const lsFavorites = getProductsFromLS("LS Favorites");
  const lsBasket = getProductsFromLS("LS Basket");

  const [products, setProducts] = useState([]);
  const [favorites, setFavorites] = useState([]);
  const [basket, setBasket] = useState(lsBasket);
  const [showModal, setShowModal] = useState(false);
  const [productToBasket, setProductTobasket] = useState({});

  useEffect(() => {
    getProducts().then((products) => {
      setProducts(products);
    });
  }, []);

  useEffect(() => {
    localStorage.setItem("LS Basket", JSON.stringify(basket));
  }, [basket]);

  useEffect(() => {
    setFavorites(lsFavorites);
  }, []);

  useEffect(() => {
    localStorage.setItem("LS Favorites", JSON.stringify(favorites));
  }, [favorites]);

  const createModal = (product) => {
    setShowModal(true);
    setProductTobasket(product);
  };

  const closeModal = () => {
    setShowModal(false);
  };

  const addToBasket = () => {
    const isBasketSquList = basket.map((el) => el.squ);
    const isInBasket = isBasketSquList.includes(productToBasket.squ);
    if (!isInBasket) {
      setBasket([...basket, productToBasket]);
      closeModal();
    }
  };

  const removeFromBasket = () => {
    const filter = basket.filter((el) => el.squ !== productToBasket.squ);
    setBasket(filter);
    closeModal();
  };

  const favoritesToggle = (squ) => {
    const favProduct = products.find((product) => product.squ === squ);
    favProduct.isFav = true;
    const isFavProduct = favorites.find((product) => product.squ === squ);
    if (isFavProduct) {
      const filter = favorites.filter((el) => el.squ !== squ);
      setFavorites(filter);
    } else {
      setFavorites([...favorites, favProduct]);
    }
  };

  return (
    <>
      <Header basketCount={basket.length} favoriteCount={favorites.length} />
      <Routes>
        <Route
          path="/"
          element={
            <>
              <div className="products-list-container">
                <h2 className="products-list-container_title">Наші товари:</h2>
                <ProductList
                  list={products}
                  openModal={createModal}
                  favoritesToggle={favoritesToggle}
                  favorites={favorites}
                  basket={basket}
                  showModal={showModal}
                  closeModal={closeModal}
                  addToBasket={addToBasket}
                  removeBtn={false}
                />
              </div>
              {showModal && (
                <Modal
                  header="Додавання у кошик"
                  text="Бажаєте додати цей товар у кошик?"
                  closeModal={closeModal}
                  addToBasket={addToBasket}
                />
              )}
            </>
          }
        />
        <Route
          path="/basket"
          element={
            <>
              <Basket
                list={basket}
                favorites={favorites}
                openModal={createModal}
                favoritesToggle={favoritesToggle}
              />
              {showModal && (
                <Modal
                  header="Видалення з кошику"
                  text="Ви бажаєте видалити цей товар із кошику?"
                  closeModal={closeModal}
                  addToBasket={removeFromBasket}
                />
              )}
            </>
          }
        />
        <Route
          path="/favorites"
          element={
            <>
              <Favorites
                openModal={createModal}
                list={favorites}
                favoritesToggle={favoritesToggle}
                basket={basket}
              />
              {showModal && (
                <Modal
                  header="Додавання у кошик"
                  text="Бажаєте додати цей товар у кошик?"
                  closeModal={closeModal}
                  addToBasket={addToBasket}
                />
              )}
            </>
          }
        />
      </Routes>
    </>
  );
};

export default App;
