import './App.scss';
import {Component} from 'react'
import Modal from './componnents/modal/modal'
import Button from './componnents/button/button';
import modalWindowDeclarations from "./modalData";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
        showModal: false,
        details: {}
      }
    }

  createModal = (e) => {
  const modalId = e.target.dataset.modalId
  const modalDeclaration = modalWindowDeclarations.find((item) => item.id === modalId)
  this.setState({
      showModal: true,
      details: modalDeclaration,
   })
  }

  closeModal = () => {
    this.setState({
      showModal: false,
      details: {}
    })
  }

  render() {
    return (
      <>
      <div className="app-container">
          <Button dataModalId="modalID1" text = "First Modal Open" backgroundColor = { '#FF2A47'} onClick={this.createModal}></Button>
          <Button dataModalId="modalID2" text = "Second Modal Open" backgroundColor = { '#57C5FF'} onClick={this.createModal}></Button>
      </div>
      {this.state.showModal && (
      <Modal stateModal={this.state.details} closeModal={this.closeModal}/>
      )}
      </>
    )
  }
}

export default App;
