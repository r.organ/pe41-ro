import { Component } from "react";
import './modal.scss';



class Modal extends Component {
    render() {

    const { header, text, actions, closeButton } = this.props.stateModal
    const { closeModal } = this.props

        return (
            <>
            <div className="modal">
                <div className="modal_title-wrapper">
                    <h3 className="modal_title-wrapper_title">{header}</h3>
                    {closeButton && (
                    <button className="modal_title-wrapper_btn" onClick={closeModal}>X</button>)}
                </div>
                <div className ="modal_body">
                    <p className="modal_body_text">{text}</p>
                    {actions}
                </div> 
            </div>
            <div className="background" onClick={closeModal} />
            </>
        )

    }
}

export default Modal