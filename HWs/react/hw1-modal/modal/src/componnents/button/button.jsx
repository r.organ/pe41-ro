import { Component } from "react";
import './button.scss'

class Button extends Component {
    
    render (){
        const {backgroundColor, text, dataModalId, onClick} = this.props 
        return (
            <button 
            className="open-btn"
            data-modal-id={dataModalId} 
            onClick={onClick} 
            style={{backgroundColor: `${backgroundColor}`}}>
                {text}</button>

        )
    }
}

export default Button