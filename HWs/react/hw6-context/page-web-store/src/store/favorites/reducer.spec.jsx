import reducer from "./reducer";

describe("favorites reducer", () => {
  const state = [
    {
      squ: 1,
      body: "product_one",
    },
    {
      squ: 2,
      body: "product_two",
    },
  ];
  it("should return default value", () => {
    expect(reducer(undefined, {})).toEqual([]);
  });

  describe("for ADD_FAVORITES actions", () => {
    it("should add product to favorites", () => {
      expect(
        reducer(state, {
          type: "ADD_FAVORITES",
          payload: { squ: 4, body: "product_four" },
        })
      ).toEqual([
        {
          squ: 1,
          body: "product_one",
        },
        {
          squ: 2,
          body: "product_two",
        },
        { squ: 4, body: "product_four" },
      ]);
    });
    it("should not add duplicate", () => {
      expect(
        reducer(state, {
          type: "ADD_FAVORITES",
          payload: { squ: 1, body: "product_one" },
        })
      ).toEqual(state);
    });
  });

  describe("for REMOVE_FAVORITES", () => {
    it("should remove from favorites", () => {
      expect(
        reducer(state, {
          type: "REMOVE_FAVORITES",
          payload: { squ: 1 },
        })
      ).toEqual([
        {
          squ: 2,
          body: "product_two",
        },
      ]);
    });
  });
});
