import reducer from "./reducer";

describe("basket reducer", () => {
  const state = [
    {
      squ: 1,
      body: "product_one",
    },
    {
      squ: 2,
      body: "product_two",
    },
  ];

  it("should return default value", () => {
    expect(reducer(undefined, {})).toEqual([]);
  });

  it("should add to basket", () => {
    expect(
      reducer(state, {
        type: "ADD_TO_BASKET",
        payload: { squ: 3, body: "product_three" },
      })
    ).toEqual([
      {
        squ: 1,
        body: "product_one",
      },
      {
        squ: 2,
        body: "product_two",
      },
      { squ: 3, body: "product_three" },
    ]);
  });

  it("should remove product from basket", () => {
    expect(
      reducer(state, {
        type: "REMOVE_FROM_BASKET",
        payload: { squ: 2 },
      })
    ).toEqual([
      {
        squ: 1,
        body: "product_one",
      },
    ]);
  });
  it("should clear basket", () => {
    expect(
      reducer(state, {
        type: "CLEAR_BASKET",
        payload: [],
      })
    ).toEqual([]);
  });
});
