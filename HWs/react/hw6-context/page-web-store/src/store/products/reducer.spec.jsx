import productsReducer from "./reducer";

describe("products reducer", () => {
  const state = {
    products: [],
    isLoading: false,
    hasError: false,
  };
  it("should return default state", () => {
    expect(productsReducer({}, {})).toEqual({});
  });

  it("should display loader", () => {
    expect(
      productsReducer(state, {
        type: "START_FETCH_PRODUCTS",
        payload: { isLoading: true },
      })
    ).toEqual({ products: [], isLoading: true, hasError: false });
  });

  it("should return list of products", () => {
    expect(
      productsReducer(state, {
        type: "LOADED_PRODUCTS",
        payload: [],
      })
    ).toEqual({
      products: [],
      isLoading: false,
      hasError: false,
    });
  });

  it("should display error if loading is failed", () => {
    expect(
      productsReducer(state, {
        type: "ERROR_LOADED_PRODUCTS",
        payload: { hasError: true },
      })
    ).toEqual({
      products: [],
      isLoading: false,
      hasError: true,
    });
  });
});
