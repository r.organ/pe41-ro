import reducer from "./reducer";

describe("modal reducer", () => {
  const state = false;
  it("should open modal", () => {
    expect(
      reducer(state, {
        type: "OPEN_MODAL",
        payload: true,
      })
    ).toEqual(true);
  });

  it("should close modal", () => {
    expect(
      reducer(state, {
        type: "CLOSE_MODAL",
        payload: false,
      })
    ).toEqual(false);
  });
});
