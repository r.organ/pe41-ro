import { render } from "@testing-library/react";
import Header from "./header";
import { Provider } from "react-redux";
import { legacy_createStore as createStore } from "redux";
import { reducer } from "../../store/store";
import { BrowserRouter } from "react-router-dom";

describe("<Header /> component", () => {
  it("should render Header", () => {
    const store = createStore(reducer, {
      basket: [
        {
          name: "11",
          price: 2222,
          squ: 2,
          url: "/ffdfd",
          color: "product",
        },
        {
          name: "1331",
          price: 2222,
          squ: 4,
          url: "/ffdfd",
          color: "pdsdsroduct",
        },
      ],
      favorites: [
        { name: "11", price: 2222, squ: 2, url: "/ffdfd", color: "product" },
        {
          name: "11",
          price: 2222,
          squ: 2,
          url: "/ffdfd",
          color: "product",
        },
      ],
      products: [
        { name: "11", price: 2222, squ: 2, url: "/ffdfd", color: "product" },
      ],
    });
    const { container } = render(
      <BrowserRouter>
        <Provider store={store}>
          <Header />
        </Provider>
      </BrowserRouter>
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
