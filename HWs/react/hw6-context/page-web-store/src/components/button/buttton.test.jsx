import { render, screen } from "@testing-library/react";
import Button from "./button";
import userEvent from "@testing-library/user-event";

describe("<Button/> component", () => {
  const handleClick = jest.fn();
  it("should render button tag", () => {
    render(
      <Button onClick={handleClick} className="btn" text="sign up"></Button>
    );

    screen.getByRole("button");
  });

  it("should render button with text", () => {
    render(
      <Button text="login" className="btn" onClick={handleClick}></Button>
    );
    screen.getByText(/login/i);
  });

  it("should pass onClick if button is clicked", () => {
    render(
      <Button onClick={handleClick} className="btn" text="submit"></Button>
    );
    userEvent.pointer({
      keys: "[MouseLeft]",
      target: screen.getByRole("button"),
    });
    expect(handleClick).toHaveBeenCalled();
  });
});


