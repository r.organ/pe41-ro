import ProductCard from "./productCard";
import "./productList.scss";
import PropTypes from "prop-types";
import { UseViewContext } from "../../services/productsViewContext";

const ProductList = ({ list, removeBtn }) => {
  const { listview } = UseViewContext();
  const isCardView = listview.view === "card";
  return (
    <>
      <div
        className={isCardView ? "list-container-card" : "list-container-table"}
      >
        {list.map((product) => {
          return (
            <ProductCard
              key={product.squ}
              product={product}
              removeBtn={removeBtn}
              isCardView={isCardView}
            />
          );
        })}
      </div>
    </>
  );
};

ProductList.propTypes = {
  list: PropTypes.array.isRequired,
};

export default ProductList;
