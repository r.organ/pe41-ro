import { render } from "@testing-library/react";
import Modal from "./modal";
import { Provider } from "react-redux";
import { legacy_createStore as createStore } from "redux";
import { reducer } from "../../store/store";

describe("<Modal /> component", () => {
  it("should render modal", () => {
    const store = createStore(reducer, {
      selectedProduct: [
        { name: "11", price: 2222, squ: 2, url: "/ffdfd", color: "product" },
      ],
    });

    render(
      <Provider store={store}>
        <Modal header="modal title" text="modal text" />
      </Provider>
    );
  });
});
