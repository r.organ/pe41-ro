import { createContext, useState, useContext } from "react";

const ViewContext = createContext();

export const UseViewContext = () => {
  return useContext(ViewContext);
};

export const ViewContextProvider = (props) => {
  const [view, setView] = useState("card");

  const toogleView = () => {
    // view === "card" ? setView("table") : setView("card");
    if (view === "card") {
      setView("table");
    } else {
      setView("card");
    }
  };

  const AppContextObj = {
    listview: { view, setView },
    toogleView: toogleView,
  };
  //   console.log(view);

  return (
    <ViewContext.Provider value={AppContextObj}>
      {props.children}
    </ViewContext.Provider>
  );
};
