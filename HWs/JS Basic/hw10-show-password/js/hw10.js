const form = document.querySelector('.password-form');
const iconEnterPassword = document.getElementById('icon');
const inputEnterPassword = document.querySelector('.enter-input')
const iconConfirmPassword = document.getElementById('confirm-icon');
const inputConfirmPassword = document.querySelector('.confirm-input')
const btn = document.querySelector('.btn')

function showPassword (elem, input) {
    form.addEventListener('click', function (e) {
        let event = e.target
        e.preventDefault();

        if (event.id === elem.id) {
            if (input.getAttribute('type') === 'password') {
                elem.className = "fas fa-eye-slash icon-password";
                input.removeAttribute('type');
                input.setAttribute('type', 'text');
            } else {
                elem.className = 'fas fa-eye icon-password';
                input.removeAttribute('type');
                input.setAttribute('type', 'password');
            }
        }
    })
}
showPassword(iconEnterPassword,inputEnterPassword)
showPassword(iconConfirmPassword,inputConfirmPassword)


function submitPassword (inputOne, inputTwo, button) {
    btn.addEventListener('click', function (e){
        let event = e.target
        let showmsg = document.createElement('p')
        e.preventDefault()

        if (event.id === button.id) {
            if (inputOne.value !== inputTwo.value) {
                showmsg.textContent = 'Нужно ввести одинаковые значения';
                showmsg.style.cssText = 'color:red'
                btn.before(showmsg)
            } else {
                showmsg.remove()
                alert('You are welcome')
            }
        }
    })
}
submitPassword(inputEnterPassword,inputConfirmPassword,btn)


