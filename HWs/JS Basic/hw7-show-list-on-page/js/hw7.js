const arrayOne = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
const arrayTwo = ["1", "2", "3", "sea", "user", 23];

function returnList(list, parent = document.body) {
    const li = list.map(elem => `<li>${elem}</li>`)
    const ul = document.createElement('ul')

    li.forEach(element => ul.innerHTML += element)
    parent.append(ul)
    setTimeout(() => ul.remove(), 3000);
}

returnList(arrayOne, document.body);
returnList(arrayTwo);
