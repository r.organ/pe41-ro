// Считать два числа, m и n. Вывести в консоль все простые числа
// в диапазоне от m до n (меньшее из введенных чисел будет m, бОльшее будет n).
// Если хотя бы одно из чисел не соблюдает условие валидации, указанное выше, вывести сообщение об ошибке,
// и спросить оба числа заново.

let m = +prompt('Enter start number of range:');
let n = +prompt('Enter last number of range:');

while (m >= n) {
    if (m > n) {
        alert(`Start number "${m}" is > than the last one "${n}". Enter again`)
    } else if (m === n) {
        alert(`Start number "${m}" is = to the last one "${n}". Enter again`)
    }
    m = +prompt('Enter start number of range:');
    n = +prompt('Enter last number of range:');
}

simpleNumber:
    for (m; m <= n; m++) {
        for (let i = 2; i < m; i++) {
            if (m % i === 0) continue simpleNumber;
        }
        console.log(m);
    }