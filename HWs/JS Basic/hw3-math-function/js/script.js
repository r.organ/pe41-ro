let firstNumber = +prompt('Please enter a first number:');
while (isNaN(firstNumber)) {
    firstNumber = +prompt(`Incorrect Number ->"${firstNumber}". Please enter again:`);
}
let secondNumber = +prompt('Please enter a second number:');
while (isNaN(secondNumber)) {
    secondNumber = +prompt(`Incorrect Number ->"${secondNumber}". Please enter again:`)
}

let operation = prompt('Please enter a math operation *, /, + or -:')
while (operation !== '+' && operation !== '-' && operation !== '*' && operation !== '/') {
    operation = prompt('Incorrect math operation. Please enter again:')
}

function getMathOperation(numberOne, numberTwo, mathOperation){
    let result;
    switch (mathOperation) {
        case '+':
            result = numberOne + numberTwo
            break
        case '-':
            result = numberOne - numberTwo
            break
        case '*':
            result = numberOne * numberTwo
            break
        case '/':
            if (numberTwo === 0) {
               result =  'Second number is 0. It"s impossible to divide on zero'
            } else {
                result = numberOne / numberTwo
            }
            break
    }
    return result;
}

console.log(`The result of ${firstNumber} ${operation} ${secondNumber} is ${getMathOperation(firstNumber, secondNumber, operation)}`)
