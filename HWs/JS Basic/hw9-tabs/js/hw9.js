let menu = document.querySelectorAll(".tabs-title");
let menuText = document.querySelectorAll(".tabs-text");

menu.forEach(e => e.classList.remove('active'));
menuText.forEach(e => e.style.display = 'none');

function tabs(nav, content) {
    for (let i = 0; i < nav.length; i++) {

        nav[i].addEventListener("click", function (e) {
            e.preventDefault();

            let activeNavAttr = e.target.getAttribute("data-title");

            for (let j = 0; j < menuText.length; j++) {
                let contentAttr = menuText[j].getAttribute("data-text");

                if (activeNavAttr === contentAttr) {
                    nav[j].classList.add("active");
                    content[j].style.display = "flex";
                } else {
                    nav[j].classList.remove("active");
                    content[j].style.display = 'none';
                }
            }
        });
    }
}
tabs(menu, menuText)
