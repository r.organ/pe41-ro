const div = document.createElement('div')
div.style.cssText = 'margin: 10px 0 5px 30px; padding: 10px; ' +
    'background-color:#b8b8b9; display:inline-block; border-radius:10px';

const priceTitle = document.createElement('p')
priceTitle.textContent = 'Price';
priceTitle.style.cssText = ' color: white'

const priceField = document.createElement('input');
priceField.type = 'number';
priceField.style.marginLeft = '15px';


div.prepend(priceTitle)
priceTitle.append(priceField)

priceField.addEventListener('focus', function(){
    priceField.style.cssText =  'border: 3px solid green; border-radius: 2px; margin-left: 15px';
} )

document.body.append(div)

const spanDiv = document.createElement('div');
spanDiv.style.cssText = 'margin: 5px 0 0 30px; padding:10px; ' +
    'border-radius:10px; display:block';

const spanBtn = document.createElement('button');
spanBtn.textContent = 'X';
spanBtn.style.cssText = 'border: 1px solid #b8b8b9; border-radius:5px'

const span = document.createElement('span');
span.style.cssText = 'margin-right: 15px'

const alertMsg = document.createElement('p');
alertMsg.textContent = 'Please enter correct price';
alertMsg.style.cssText = 'margin-inline-start: 30px'

const emptyInput = document.createElement('p');
emptyInput.textContent = 'Please enter price';
emptyInput.style.cssText = 'margin-inline-start: 30px'

priceField.addEventListener('blur', function(){
    priceField.style.cssText =  'border: ""; color: green; margin-left: 15px';

    if (priceField.value < 0) {
        priceField.style.cssText =  'margin-left: 15px; border: 2px solid red';
        div.after(alertMsg);
        emptyInput.remove();
    }
    else if (priceField.value === ""){
     div.after(emptyInput)
    }
    else {
        span.textContent = (`Текущая цена: ${priceField.value}`);
        spanDiv.prepend(span, spanBtn)
        div.before(spanDiv)

    }
} )

window.addEventListener('click', function(){
    if (priceField.value > 0) {
        alertMsg.remove();
        emptyInput.remove();
    } else if (priceField.value < 0 ) {
        emptyInput.remove();} else if (priceField.value === "") {
        alertMsg.remove();
    }

})

spanBtn.addEventListener('click', function(){
    spanDiv.remove()
    priceField.value = '';
})


