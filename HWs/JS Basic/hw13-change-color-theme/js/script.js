const form = document.querySelector('.sign-up-form');
const formFields = document.querySelectorAll('.form-field')
const themeBtn = document.getElementById('change-theme');

function darkTheme () {
    form.style.background = 'grey';
    document.querySelector('.title-form-signup').style.color = 'white';
    document.querySelector('.or').style.color = 'white';
    document.getElementById('tc').style.color = 'white';
    formFields.forEach((e) =>  {
        e.style.cssText = 'color: white; background-color:grey'
    })
    localStorage.setItem('theme', 'dark');
}

function lightTheme () {
    form.style.background = 'white';
    document.querySelector('.title-form-signup').style.color = '#434343';
    document.querySelector('.or').style.color = '#848484';
    document.getElementById('tc').style.color = '#434343';
    formFields.forEach((e) =>  {
        e.style.cssText = 'color: #BEBEBE; background-color:white'
    })
    localStorage.setItem('theme', 'light');
}

themeBtn.onclick = function () {
    if (form.style.background === 'grey') {
        lightTheme()
    } else darkTheme()
}

window.onload = function () {
    if (localStorage.getItem('theme') === 'dark') {
    darkTheme()
    }
    if (localStorage.getItem('theme') === 'light') {
    lightTheme()
    }
}