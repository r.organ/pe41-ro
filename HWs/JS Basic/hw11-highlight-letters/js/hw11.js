const btn = document.querySelectorAll('.btn')

btn.forEach(function(key) {
    window.addEventListener('keyup', (event)=> {
        let pressedkey = event.key;
        if (pressedkey.length === 1) {
            pressedkey = pressedkey.toUpperCase()
        }
        if (key.innerText === pressedkey) {
            key.style.cssText = 'background-color: blue'
        } else key.style.cssText = 'background-color: #000000'
    })
})
