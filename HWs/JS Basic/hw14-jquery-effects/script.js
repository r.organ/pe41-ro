$(document).ready(function () {

    $("body").on('click', '[href*="#"]', function (e) {
        $('html,body').stop().animate({scrollTop: $(this.hash).offset().top}, 1500);
        e.preventDefault();
    });


    $(window).on('scroll', function () {
        if ($(this).scrollTop() > window.screen.availHeight) {
            if ($('#upbutton').is(':hidden')) {
                $('#upbutton').css({opacity: 1}).fadeIn('slow');
            }
        } else {
            $('#upbutton').stop(true, false).fadeOut('fast');
        }
    });
    $('#upbutton').on('click', function () {
        $('html, body').stop().animate({scrollTop: 0}, 300);
    });


    $(document).on('click', '#toggleBtn', function (){
        let btn = $(this);
        $('html, body').stop().animate({scrollTop: 0}, 600);
        $('.posts-list').slideToggle('slow', function (){
            if ($(this).is(":visible")) {
                btn.text('Hide section');
            } else {
                btn.text('Show section')
            }
        });
    })

});

