const imgList = document.querySelectorAll('.image-to-show')

let curImg = 0;

let timer = setInterval(showImg, 3000);
const btnStop = document.querySelector('#btn-stop');
const btnPlay = document.querySelector('#btn-play');

function showImg() {
    imgList[curImg].style.display = 'none';
    curImg = (curImg + 1) % imgList.length;
    imgList[curImg].style.display = 'block';
}

btnStop.addEventListener('click', function(){
    clearInterval(timer);
})

btnPlay.addEventListener('click', function(){
    timer = setInterval(showImg, 3000);
})
